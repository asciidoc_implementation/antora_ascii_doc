---
title: Управление
description: Основные команды управления узлом PROTEI MME
type: docs
weight: 20
---

= Управление

ifeval::["{backend}" == "html5"]
include::partial$html5s.adoc[]
endif::[]

== Директории

В PROTEI MME используются следующие директории:

[horizontal]
*_/usr/protei/Protei_MME_*:: рабочая директория;
*_/usr/protei/Protei_MME/bin_*:: директория для исполняемых файлов;
*_/usr/protei/Protei_MME/cdr_*:: директория для CDR--журналов;
*_/usr/protei/Protei_MME/config_*:: директория для конфигурационных файлов;
*_/usr/protei/Protei_MME/log_*:: директория для лог-файлов;
*_/usr/protei/Protei_MME/metrics_*:: директория для файлов метрик;
*_/usr/protei/Protei_MME/scripts_*:: директория для скриптов, реализующих запросы API;

== Управляющие команды

* Чтобы запустить PROTEI MME, следует выполнить одну из команд:
 ** команду *_systemctl start_* от лица суперпользователя:

[source,bash]
----
$ sudo systemctl start mme
----

* скрипт *_start_* в рабочей папке:

[source,bash]
----
$ /usr/protei/Protei_MME/start
----

* Чтобы остановить PROTEI MME, следует выполнить одну из команд:
 ** команду *_systemctl stop_* от лица суперпользователя:

[source,bash]
----
$ sudo systemctl stop mme
----

* скрипт *_stop_* в рабочей папке:

[source,bash]
----
$ /usr/protei/Protei_MME/stop
----

* Чтобы проверить текущее состояние PROTEI MME, следует выполнить команду *_systemctl status_* от лица суперпользователя:

[source,console]
----
$ sudo systemctl status mme
● mme.service - MME
   Loaded: loaded (/usr/lib/systemd/system/mme.service; enabled; vendor preset: disable>
  Drop-In: /etc/systemd/system/mme.service.d
           └─override.conf
   Active: active (running) since Fri 2023-07-21 19:50:25 MSK; 2 days ago
  Process: 998814 ExecStopPost=/usr/protei/Protei_MME/bin/utils/check_history.sh (code=exited, status=0/SUCCESS)
  Process: 998729 ExecStopPost=/usr/protei/Protei_MME/bin/utils/move_log.sh (code=exited, status=0/SUCCESS)
  Process: 998683 ExecStop=/usr/protei/Protei_MME/bin/utils/stop_prog.sh (code=exited, status=0/SUCCESS)
  Process: 999135 ExecStart=/usr/protei/Protei_MME/bin/utils/start_prog.sh (code=exited, status=0/SUCCESS)
  Process: 999108 ExecStartPre=/usr/protei/Protei_MME/bin/utils/check_history.sh (code=exited, status=0/SUCCESS)
  Process: 999082 ExecStartPre=/usr/protei/Protei_MME/bin/utils/move_log.sh (code=exited, status=0/SUCCESS)
 Main PID: 999151 (Protei_MME)
    Tasks: 11 (limit: 35612)
   Memory: 489.7M
   CGroup: /system.slice/mme.service
           └─999151 ./bin/Protei_MME
----

* Чтобы проверить текущую версию PROTEI MME, следует запустить скрипт *_version_* в рабочей папке:

[source,console]
----
$ ./version
Start: Protei_MME
MME: Company: Protei
Copyright:
Product: Protei_MME
ProductCode: 1.46.2.0
BuildNumber: 997
Label:
AdditionalInfo: Supported license
 Release
CVS_Version:
SVN_Version:
GIT_Version: https://git.protei.ru/MobileDevelop/Protei_MME.git HEAD 648c530dab58568bebc5468c2acb270aee654e40
VersionDate: Sep 20 2023 19:40:10
----

* Чтобы перезагрузить PROTEI MME, следует выполнить одну из команд:
 ** команду *_systemctl restart_* от лица суперпользователя:

[source,console]
----
$ sudo systemctl restart mme
● mme.service - MME
   Loaded: loaded (/usr/lib/systemd/system/mme.service; enabled; vendor preset: disabled)
   Active: active (running) since Fri 2023-07-21 19:50:25 MSK; 2 days ago
  Process: 99426 ExecStopPost=/usr/protei/Protei_MME/bin/utils/check_history.sh (code=exited, status=0/SUCCESS)
  Process: 99344 ExecStopPost=/usr/protei/Protei_MME/bin/utils/move_log.sh (code=exited, status=0/SUCCESS)
  Process: 99300 ExecStop=/usr/protei/Protei_MME/bin/utils/stop_prog.sh (code=exited, status=0/SUCCESS)
  Process: 99508 ExecStart=/usr/protei/Protei_MME/bin/utils/start_prog.sh (code=exited, status=0/SUCCESS)
  Process: 99481 ExecStartPre=/usr/protei/Protei_MME/bin/utils/check_history.sh (code=exited, status=0/SUCCESS)
  Process: 99453 ExecStartPre=/usr/protei/Protei_MME/bin/utils/move_log.sh (code=exited, status=0/SUCCESS)
 Main PID: 99524 (Protei_MME)
    Tasks: 1 (limit: 35612)
   Memory: 2.5M
   CGroup: /system.slice/mme.service
           └─99524 ./bin/Protei_MME --mme-standalone
----

* скрипт *_restart_* в рабочей папке:

[source,bash]
----
$ sh /usr/protei/Protei_MME/restart
----

* Чтобы перезагрузить конфигурационный файл *_file.cfg_*, следует запустить скрипт *_reload_* в рабочей папке:

[source,console]
----
$ /usr/protei/Protei_MME/reload <file.cfg>
reload <file> config Ok
----

* Чтобы записать дамп ядра, следует запустить скрипт *_core_dump_* в рабочей папке:

[source,console]
----
$ /usr/protei/Protei_MME/core_dump
Are you sure you want to continue? y
Core dump generated!
----

NOTE: Файл дампа хранится в директории *_/var/lib/systemd/coredump_*.