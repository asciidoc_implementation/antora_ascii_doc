---
title: metrics.cfg
description: Параметры сбора метрик
type: docs
weight: 20
---

= metrics.cfg

ifeval::["{backend}" == "html5"]
include::partial$html5s.adoc[]
endif::[]

В файле задаются настройки сбора метрик.

.Используемые секции
[horizontal]
<<general,[General]>>:: общие параметры сбора метрик
<<s1Interface,[s1interface]>>:: параметры сбора метрик S1
<<s1Attach,[s1attach]>>:: параметры сбора метрик S1, связанных с регистрацией абонента
<<s1Detach,[s1detach]>>:: параметры сбора метрик S1, связанных с дерегистрацией абонента
<<s1BearerActivation,[s1beareractivation]>>:: параметры сбора метрик S1, связанных с активацией bearer-служб
<<s1BearerDeactivation,[s1bearerdeactivation]>>:: параметры сбора метрик S1, связанных с деактивацией bearer-служб
<<s1BearerModification,[s1bearermodification]>>:: параметры сбора метрик S1, связанных с модификацией bearer-служб
<<s1Security,[s1security]>>:: параметры сбора метрик S1, связанных с аутентификацией абонента
<<s1Service,[s1service]>>:: параметры сбора метрик S1, связанных с процедурой Service Request
<<handover,[handover]>>:: параметры сбора метрик, связанных с процедурой Handover
<<tau,[tau]>>:: параметры сбора метрик, связанных с процедурой Tracking Area Update
<<sgsInterface,[sgsInterface]>>:: параметры сбора метрик SGs
<<svInterface,[svInterface]>>:: параметры сбора метрик Sv (SRVCC HO)
<<resource,[resource]>>:: параметры сбора метрик, связанных с ресурсами хоста
<<users,[users]>>:: параметры сбора различных счётчиков
<<s11Interface,[s11interface]>>:: параметры сбора метрик S11
<<paging,[paging]>>:: параметры сбора метрик, связанных с процедурой Paging

.Описание параметров
[options="header",cols="4,8,2,1,1,2"]
|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| *[[general]][General]*
| Общие параметры сбора метрик.
|
| O
| P
| 1.45.0.0

| Enable
| Флаг сбора метрик. +
По умолчанию: 0.
| bool
| O
| P
| 1.45.0.0

| Directory
| Директория для хранения метрик. +
По умолчанию: ../metrics.
| string
| O
| P
| 1.45.0.0

| [[granularity]]Granularity
| Гранулярность сбора метрик, в минутах. +
По умолчанию: 5.
| int
| O
| P
| 1.45.0.0

| [[node_name]]NodeName
| Имя узла, указываемое в названии файла с метриками.
| string
| O
| P
| 1.45.0.0

| *[[s1Interface]][s1Interface]*
| Параметры для метрик S1.
|
| O
| P
| 1.45.0.0

| Granularity
| Гранулярность сбора метрик, в минутах. +
По умолчанию: значение <<granularity,[General]::Granularity>>.
| int
| O
| P
| 1.45.0.0

| *[[s1Attach]][s1Attach]*
| Параметры для метрик S1, связанных с регистрацией абонента.
|
| O
| P
| 1.45.0.0

| Granularity
| Гранулярность сбора метрик, в минутах. +
По умолчанию: значение <<granularity,[General]::Granularity>>.
| int
| O
| P
| 1.45.0.0

| *[[s1Detach]][s1Detach]*
| Параметры для метрик S1, связанных с дерегистрацией абонента.
|
| O
| P
| 1.46.0.0

| Granularity
| Гранулярность сбора метрик, в минутах. +
По умолчанию: значение <<granularity,[General]::Granularity>>.
| int
| O
| P
| 1.46.0.0

| *[[s1BearerActivation]][s1BearerActivation]*
| Параметры для метрик S1, связанных с активацией bearer-службы.
|
| O
| P
| 1.45.0.0

| Granularity
| Гранулярность сбора метрик, в минутах. +
По умолчанию: значение <<granularity,[General]::Granularity>>.
| int
| O
| P
| 1.45.0.0

| *[[s1BearerDeactivation]][s1BearerDeactivation]*
| Параметры для метрик S1, связанных с деактивацией bearer-службы.
|
| O
| P
| 1.45.0.0

| Granularity
| Гранулярность сбора метрик, в минутах. +
По умолчанию: значение <<granularity,[General]::Granularity>>.
| int
| O
| P
| 1.45.0.0

| *[[s1BearerModification]][s1BearerModification]*
| Параметры для метрик S1, связанных с модификацией bearer-службы.
|
| O
| P
| 1.46.0.0

| Granularity
| Гранулярность сбора метрик, в минутах. +
По умолчанию: значение <<granularity,[General]::Granularity>>.
| int
| O
| P
| 1.46.0.0

| *[[s1Security]][s1Security]*
| Параметры для метрик S1, связанных с аутентификацией абонента.
|
| O
| P
| 1.46.0.0

| Granularity
| Гранулярность сбора метрик, в минутах. +
По умолчанию: значение <<granularity,[General]::Granularity>>.
| int
| O
| P
| 1.46.0.0

| *[[s1Service]][s1Service]*
| Параметры для метрик S1, связанных с процедурой Service Request.
|
| O
| P
| 1.46.0.0

| Granularity
| Гранулярность сбора метрик, в минутах. +
По умолчанию: значение <<granularity,[General]::Granularity>>.
| int
| O
| P
| 1.46.0.0

| *[[handover]][handover]*
| Параметры для метрик, связанных с процедурой Handover.
|
| O
| P
| 1.46.0.0

| Granularity
| Гранулярность сбора метрик, в минутах. +
По умолчанию: значение <<granularity,[General]::Granularity>>.
| int
| O
| P
| 1.46.0.0

| *[[tau]][tau]*
| Параметры для метрик, связанных с процедурой Tracking Area Update.
|
| O
| P
| 1.46.0.0

| Granularity
| Гранулярность сбора метрик, в минутах. +
По умолчанию: значение <<granularity,[General]::Granularity>>.
| int
| O
| P
| 1.46.0.0

| *[[sgsInterface]][sgsInterface]*
| Параметры для метрик SGs.
|
| O
| P
| 1.46.0.0

| Granularity
| Гранулярность сбора метрик, в минутах. +
По умолчанию: значение <<granularity,[General]::Granularity>>.
| int
| O
| P
| 1.46.0.0

| *[[svInterface]][svInterface]*
| Параметры для метрик Sv (SRVCC HO).
|
| O
| P
| 1.46.0.0

| Granularity
| Гранулярность сбора метрик, в минутах. +
По умолчанию: значение <<granularity,[General]::Granularity>>.
| int
| O
| P
| 1.46.0.0

| *[[resource]][resource]*
| Параметры для метрик, связанных с ресурсами хоста.
|
| O
| P
| 1.46.0.0

| Granularity
| Гранулярность сбора метрик, в минутах. +
По умолчанию: значение <<granularity,[General]::Granularity>>.
| int
| O
| P
| 1.46.0.0

| *[[users]][users]*
| Параметры сбора различных счётчиков.
|
| O
| P
| 1.46.0.0

| Granularity
| Гранулярность сбора метрик, в минутах. +
По умолчанию: значение <<granularity,[General]::Granularity>>.
| int
| O
| P
| 1.46.0.0

| *[[s11Interface]][s11Interface]*
| Параметры для метрик S11.
|
| O
| P
| 1.45.0.0

| Granularity
| Гранулярность сбора метрик, в минутах. +
По умолчанию: значение <<granularity,[General]::Granularity>>.
| int
| O
| P
| 1.45.0.0

| *[[paging]][paging]*
| Параметры для метрик, связанных с процедурой Paging.
|
| O
| P
| 1.46.0.0

| Granularity
| Гранулярность сбора метрик, в минутах. +
По умолчанию: значение <<granularity,[General]::Granularity>>.
| int
| O
| P
| 1.46.0.0

| *[[s6a_interface]][S6a-interface]*
| Параметры для метрик S6a.
|
| O
| P
| 1.45.0.0

| Granularity
| Гранулярность сбора метрик, в минутах. +
По умолчанию: значение <<granularity,[General]::Granularity>>.
| int
| O
| P
| 1.45.0.0
|===

.Пример
[source,ini]
----
[General]
Enable = 1;
Granularity = 30;
Directory = "/usr/protei/Protei_MME/metrics";
NodeName = MME-1;

[Diameter]
Enable = 1;
Granularity = 15;

[S6a-interface]
Enable = 1;
Granularity = 1;
----