---
title: diam_dest.cfg
description: Параметры IMSI, Destination-Host и Destination-Realm
type: docs
weight: 20
---

= diam_dest.cfg

ifeval::["{backend}" == "html5"]
include::partial$html5s.adoc[]
endif::[]

В файле задаются правила, связывающие IMSI и параметры Destination-Host и Destination-Realm. Каждой связи соответствует одной секцией.

NOTE: Наличие файла обязательно.

Ключ для перегрузки -- *reload diam_dest.cfg*.

.Описание параметров
[options="header",cols="4,8,2,1,1,2"]
|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| IMSI
| Номер или маска IMSI.
| string/regex
| M
| R
|

| DestHost
| Хост назначения.
| string
| O
| R
|

| [[dest_realm]]DestRealm
| Realm назначения.
| string
| O
| R
|

| FeatureList1
| Битовая маска опций Feature-List-ID 1. Cм. https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf[3GPP TS 29.272]. +
По умолчанию: 0x10000007.
| binary
| O
| R
| 1.17.0.0

| FeatureList2
| Битовая маска опций Feature-List-ID 2. Cм. https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf[3GPP TS 29.272]. +
По умолчанию: 0x08000000.
| binary
| O
| R
| 1.17.0.0
|===

.Пример
[source,console]
----
{
  IMSI = "001010000000315$";
  DestHost = "hss.epc.mnc02.mcc001.3gppnetwork.org";
  DestRealm = "epc.mnc02.mcc001.3gppnetwork.org";
  FeatureList1 = "0x10000207";
  FeatureList2 = "0x08000011";
}
----