---
title: "sgsap.cfg"
description: "Параметры компонента SGsAP"
weight: 20
type: docs
---

В файле задаются настройки компонентов SGsAP и SGsAP.PCSM.
Может быть лишь один компонент первого типа, однако компонентов второго типа может быть несколько.

**Примечание.** Наличие файла обязательно.

### Описание параметров Sg.SGsAP {#sgsap}

| Параметр                 | Описание                                   | Тип        | O/M | P/R | Версия |
|--------------------------|--------------------------------------------|------------|-----|-----|--------|
| ComponentAddr            | Адрес компонента.                          | string     | M   | R   |        |
| ComponentType            | Тип компонента.<br>По умолчанию: Sg.SGsAP. | string     | O   | R   |        |
| Params                   | Параметры компонента.                      | object     | M   | R   |        |
| **{**                    |                                            |            |     |     |        |
| [PeerTable](#peer_table) | Таблица пиров.                             | \[object\] | O   | R   |        |
| DefaultPCSM              | Имя узла PCSM по умолчанию.                | \[str\]    | O   | R   |        |
| **}**                    |                                            |            |     |     |        |

#### Параметры элемента таблицы PeerTable {#peer-table}

* Для пира с одним IP-адресом:

| Параметр | Описание                                       | Тип    | O/M | P/R | Версия |
|----------|------------------------------------------------|--------|-----|-----|--------|
| PeerIP   | IP-адрес пира.                                 | ip     | M   | R   |        |
| GT       | Глобальный заголовок пира.                     | string | M   | R   |        |
| PCSM     | Компонентный адрес соответствующего узла PCSM. | string | M   | R   |        |
| PeerName | Имя пира.                                      | string | O   | R   |        |

* Для пира с несколькими IP-адресами:

| Параметр  | Описание                                       | Тип        | O/M | P/R | Версия  |
|-----------|------------------------------------------------|------------|-----|-----|---------|
| GT        | Глобальный заголовок пира.                     | string     | M   | R   | 1.1.0.0 |
| PeerName  | Имя пира.                                      | string     | O   | R   | 1.1.0.0 |
| PCSM_list | Набор параметров отдельных PCSM.               | \[object\] | M   | R   | 1.1.0.0 |
| **{**     |                                                |            |     |     |         |
| PeerIP    | IP-адрес пира.                                 | ip         | M   | R   | 1.1.0.0 |
| PCSM      | Компонентный адрес соответствующего узла PCSM. | string     | M   | R   | 1.1.0.0 |
| Priority  | Приоритет.<br>По умолчанию: 0 (высший).        | int        | O   | R   | 1.1.0.0 |
| Weight    | Вес.<br>По умолчанию: 1.                       | int        | O   | R   | 1.1.0.0 |
| **}**     |                                                |            |     |     |         |

### Описание параметров Sg.SGsAP.PCSM ###

| Параметр                         | Описание                                                                                                       | Тип         | O/M | P/R | Версия   |
|----------------------------------|----------------------------------------------------------------------------------------------------------------|-------------|-----|-----|----------|
| ComponentAddr                    | Адрес компонента.                                                                                              | string      | M   | R   |          |
| ComponentType                    | Тип компонента.<br>По умолчанию: Sg.SGsAP.PCSM.                                                                | string      | O   | R   |          |
| Params                           | Параметры компонента.                                                                                          | object      | M   | R   |          |
| **{**                            |                                                                                                                |             |     |     |          |
| <a name="peer-ip">PeerIP</a>     | IP-адрес подключения узла PCSM.                                                                                | ip          | M   | R   |          |
| <a name="peer-port">PeerPort</a> | Порт подключения узла PCSM.                                                                                    | string      | M   | R   |          |
| <a name="src-ip">SrcIP</a>       | Локальный IP-адрес узла PCSM.<br>По умолчанию: [sgsap::\[LocalAddress\]::LocalHost](../../sgsap/#local-host/). | string      | O   | R   |          |
| <a name="src-port">SrcPort</a>   | Локальный порт узла PCSM.<br>По умолчанию: [sgsap::\[LocalAddress\]::LocalPort](../../sgsap/#local-port/).     | string      | O   | R   |          |
| RemoteInterfaces                 | Удаленные адреса для Multihoming. Формат:<br>`{ "<ip>:<port>"; "<ip>:<port>"; }`                               | \[ip:port\] | O   | R   |          |
| LocalInterfaces                  | Локальные адреса для Multihoming. Формат:<br>`{ "<ip>:<port>"; "<ip>:<port>"; }`                               | \[ip:port\] | O   | R   | 1.37.2.0 |
| **}**                            |                                                                                                                |             |     |     |          |

**Примечание.** При значениях `PeerIP = ""` и `PeerPort = 0` PCSM ожидает подключения с адреса, указанного в описании PCSM в секции [Sg.SGsAP](#sgsap).

#### Пример ####

```
{
  ComponentAddr = Sg.SGsAP;
  ComponentType = Sg.SGsAP;
  Params = {
    PeerTable = {
      {
        PeerIP = "192.168.125.154";
        GT = "79216567568";
        PCSM = "Sg.SGsAP.PCSM.0";
      };
      {
        GT = "79216561234";
        PCSM_list = {
          {
            PeerIP = "192.168.126.155";
            PCSM = "Sg.SGsAP.PCSM.1";
            Weight = 1;
            Priority = 1;
          };
          {
            PeerIP = "192.168.126.156";
            PCSM = "Sg.SGsAP.PCSM.2";
            Weight = 2;
            Priority = 1;
          };
        };
        PeerName = "MultiIP_Peer";
      };
    };
    DefaultPCSM = {
     "Sg.SGsAP.PCSM.0";
    };
  };
}

{
  ComponentAddr = Sg.SGsAP.PCSM.0;
  ComponentType = Sg.SGsAP.PCSM;
  Params = {
    PeerIP = "192.168.125.154";
    PeerPort = 29118;
    SrcIP = "192.168.126.67";
    SrcPort = 29119;
  };
}

{
  ComponentAddr = Sg.SGsAP.PCSM.1;
  ComponentType = Sg.SGsAP.PCSM;
  Params = {
    PeerIP = "192.168.126.155";
    PeerPort = 29118;
    SrcIP = "192.168.126.67";
    SrcPort = 29119;
  };
}

{
  ComponentAddr = Sg.SGsAP.PCSM.2;
  ComponentType = Sg.SGsAP.PCSM;
  Params = {
    PeerIP = "192.168.126.156";
    PeerPort = 29118;
    SrcIP = "192.168.126.67";
    SrcPort = 29119;
  };
}
```