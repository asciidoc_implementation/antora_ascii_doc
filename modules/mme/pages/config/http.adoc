---
title: http.cfg
description: Параметры HTTP
type: docs
weight: 20
---

= http.cfg

ifeval::["{backend}" == "html5"]
include::partial$html5s.adoc[]
endif::[]

В файле задаются настройки HTTP-соединений.

NOTE: Наличие файла обязательно.

.Используемые секции
[horizontal]
<<server,[Server]>>:: параметры сервера
<<ssl_s,[SSL]>>::: параметры защищенного соединения;
<<auth_s,[Authorization]>>::: параметры авторизации;
<<keep_alive_s,[KeepAlive]>>::: параметры процедуры *keep-alive*;
<<client,[Client]>>:: параметры клиента
<<ssl_c,[SSL]>>::: параметры защищенного соединения;
<<auth_c,[Authorization]>>::: параметры авторизации;
<<add_dir,[AdditionalDir]>>::: параметры менеджера соединений;
<<keep_alive_c,[KeepAlive]>>::: параметры процедуры *keep-alive*;

.Серверная сторона
[options="header",cols="4,8,2,1,1,2"]
|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| *[[server]][Server]*
|
|
|
|
|

| ID
| Идентификатор направления.
| int
| M
| R
|

| Address
| Прослушиваемый IP-адрес. +
По умолчанию: 0.0.0.0.
| string
| O
| R
|

| Port
| Прослушиваемый порт. +
По умолчанию: 80.
| int
| O
| R
|

| MaxQueue
| Максимальная длина очереди запросов в одном соединении для <<persistant,Persistent>>-режима. +
По умолчанию: 10.
| int
| O
| R
|

| [[maxbuf_s]]MaxBufferSize
| Максимальный размер буфера для приема сообщений, в битах. +
NOTE: Если "-1", то не ограничен: 4&nbsp;294&nbsp;967&nbsp;295 +
По умолчанию: -1.
| int
| O
| R
|

| RecvBufferSize
| Минимальный размер буфера для приема сообщений, в битах. +
NOTE: Если сообщение превышает размер буфера, то буфер будет увеличиваться вплоть до <<maxbuf_s,MaxBufferSize>>. +
В зависимости от специфики приложений позволяет экономить ресурсы. Например, для работы с XML-приложениями в среднем требуется меньший размер буфера, чем для работы с MMS. +
По умолчанию: 65&nbsp;536.
| int
| O
| R
|

| ActivityTimer
| Время ожидания активности соединения, в секундах. +
NOTE: По истечении при отсутствии новых запросов соединение разрывается, объект *HTTP_ClientCL* уничтожается. +
По умолчанию: 60.
| int
| O
| R
|

| StreamCount
| Количество потоков сервера при соединении по HTTP/2. +
По умолчанию: 4&nbsp;294&nbsp;967&nbsp;295.
| int
| O
| R
| 1.3.0.17

| *[[ssl_s]][SSL]*
| Параметры защищенного соединения.
| object
| O
| R
|

| Version
| Версия SSL. +
*_1_* -- TLSv1 / *_2_* -- SSLv2 / *_3_* -- SSLv3 / *_4_* -- SSLv23 / *_5_* -- TLSv1.1 / *_6_* -- TLSv1.2. +
По умолчанию: 2.
| int
| O
| R
|

| CertificatePath
| Путь к файлу сертификата с расширением *.pem.
| string
| M
| R
|

| PrivateKeyPath
| Путь к файлу, содержащему секретный ключ.
| string
| M
| R
|

| Ciphers
| Cписок поддерживаемых шифров. Формат: +
*_"<cipher>:<cipher>"_*. +
NOTE: Возможные значения см. https://www.openssl.org/docs/man1.1.1/man1/ciphers.html[OpenSSL Cryptography and SSL/TLS Toolkit].
| string
| O
| R
|

| PreferServerCiphers
| Флаг приоритета шифров на стороне сервера вместо клиента. +
По умолчанию: 0.
| int
| O
| R
|

| *[[auth_s]][Authorization]*
| Параметры авторизации.
| object
| O
| R
|

| Type
| Тип авторизации. +
*_1_* -- Basic / *_2_* -- Digest (пока не поддерживается). +
По умолчанию: 1.
| int
| O
| R
|

| User
| Логин для авторизации.
| string
| M
| R
|

| Password
| Пароль для авторизации.
| string
| O
| R
|

| *[[keep_alive_s]][KeepAlive]*
| Параметры процедуры keep-alive.
| object
| O
| R
|

| PingTimeout
| Время ожидания между процедурами. +
По умолчанию: 10.
| int
| O
| R
|

| PingReqNumber
| Количество Ping-запросов для разрыва соединения. +
По умолчанию: 5.
| int
| O
| R
|
|===

.Клиентская сторона
[options="header",cols="4,8,2,1,1,2"]
|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| *[[client]][Client]*
|
|
|
|
|

| ID
| Идентификатор направления. +
NOTE: Контроль за уникальностью идентификаторов возлагается на администратора системы.
| int
| M
| R
|

| DestAddress
| IP-адрес сервера, которому должны быть предназначены запросы. Формат: +
*_"ip_address";port_* +
Начиная с v1.2.5.1, поддерживаются DNS-имена.
| {string,int} +
string
| M
| R
|

| SrcAddress
| IP-адрес для отправки запроса, если сконфигурировано несколько сетевых интерфейсов.
| string
| O
| R
|

| [[persistant]]Persistant
| Флаг активации режима использования Persistant-соединений. +
Если значение ненулевое, то после получения ответа соединение остается активным в течение <<activity_timer,ActivityTimer>>. +
Разрешается отправка запросов конвейером: клиент отправляет запросы, не дожидаясь ответа на предыдущие. +
По умолчанию: 0.
| bool
| O
| R
|

| [[activity_timer]]ActivityTimer
| Время активности соединения при активации *Persistant*-режима, в секундах. +
NOTE: По истечении, если от логики не поступило новых запросов, соединение разрывается, для всех запросов в очереди формируется оповещение логики об ошибке, затем объект *HTTP_ClientCL* уничтожается. +
По умолчанию: 60.
| int
| O
| R
|

| ResponseTimer
| Время ожидания ответа от сервера, в секундах. +
NOTE: По истечении, если не получен ответ, соединение разрывается, для всех запросов в очереди формируется оповещение логики об ошибке, затем объект *HTTP_ClientCL* уничтожается. +
По умолчанию: 60.
| int
| O
| R
|

| MaxQueue
| Максимальное количество запросов в очереди в одном TCP-соединении. +
NOTE: По достижении, интерфейс создает новый объект *HTTP_ClientCL* для нового TCP-соединения. +
По умолчанию: 10.
| int
| O
| R
|

| MaxConnection
| Максимальное количество одновременных соединений к серверу. +
NOTE: По достижении, интерфейс отвечает HTTP_ERROR_IND с кодом ошибки *_OVERLOADED_*. +
По умолчанию: 500.
| int
| O
| R
|

| [[maxbuf_c]]MaxBufferSize
| Максимальный размер буфера для приема сообщений, в битах. +
NOTE: Если "-1", то не ограничен: 4&nbsp;294&nbsp;967&nbsp;295. +
По умолчанию: -1.
| int
| O
| R
|

| RecvBufferSize
| Минимальный размер буфера для приема сообщений, в битах. +
NOTE: Если сообщение превышает размер буфера, то буфер будет увеличиваться вплоть до <<maxbuf_c,MaxBufferSize>>. +
В зависимости от специфики приложений позволяет экономить ресурсы. Например, для работы с XML-приложениями в среднем требуется меньший размер буфера, чем для работы с MMS. +
По умолчанию: 65&nbsp;536.
| int
| O
| R
|

| HTTP_Version
| Версия HTTP для создания соединения. +
*_1_* -- HTTP/1.0, HTTP/1.1 / *_2_* -- HTTP/2. +
По умолчанию: 1. +
NOTE: При использовании HTTP/2 необходимо активировать режим <<persistant,Persistant>>.
| int
| O
| R
| 1.3.0.17

| StreamCount
| Количество потоков при соединении по HTTP/2. +
По умолчанию: 4&nbsp;294&nbsp;967&nbsp;295.
| int
| O
| R
| 1.3.0.17

| *[[ssl_c]][SSL]*
| Параметры защищенного соединения.
| object
| O
| R
|

| Version
| Версия SSL. +
*_1_* -- TLSv1 / *_2_* -- SSLv2 / *_3_* -- SSLv3 / *_4_* -- SSLv23 / *_5_* -- TLSv1.1 / *_6_* -- TLSv1.2. +
По умолчанию: 0.
| int
| O
| R
|

| SNI_Enabled
| Флаг использования {SNI}. +
По умолчанию: 0.
| bool
| O
| R
|

| ALPN_Enabled
| Флаг использования {ALPN} для HTTP/2. +
По умолчанию: 0.
| bool
| O
| R
| 1.3.0.17

| *[[auth_c]][Authorization]*
| Параметры авторизации.
| object
| O
| R
|

| User
| Логин для авторизации.
| string
| M
| R
|

| Password
| Пароль для авторизации.
| string
| O
| R
|

| *[[add_dir]][AdditionalDir]*
| Параметры менеджера соединений.
| object
| O
| R
| 1.3.0.20

| ID
| Список запасных направлений. Формат: +
*_{ <direction>; <direction> }_*
| [int]
| M
| R
|

| BreakDownTimeout
| Время ожидания перезагрузки направлений, в миллисекундах. +
По умолчанию: 30&nbsp;000.
| int
| O
| R
|

| MaxErrorCount
| Максимальное количество возможных ошибок. +
По умолчанию: 1.
| int
| O
| R
|

| Necromancy
| Флаг восстановления соединений. +
По умолчанию: 0.
| bool
| O
| R
|

| UseLoadSharing
| Код типа распределения нагрузки между направлениями. +
*_0_* -- не использовать; +
*_1_* -- распределить между всеми; +
*_2_* -- только между дополнительными при неактивном основном. +
По умолчанию: 0.
| int
| O
| R
| 1.2.8.15

| *[[keep_alive_c]][KeepAlive]*
| Параметры для процедуры keep-alive.
| object
| O
| R
|

| PingTimeout
| Время ожидания между процедурами. +
По умолчанию: 10.
| int
| O
| R
|

| PingReqNumber
| Количество Ping-запросов для разрыва соединения. +
По умолчанию: 5.
| int
| O
| R
|
|===

.Пример
[source,textmate]
----
[Server]
MaxBufferSize = 524626
ActivityTimer = 10
{
  ID = 0;
  Address = "195.218.228.2";
  Port = 80;
  SSL = {
    Version = 1;
    CertificatePath = "/home/usr/Projects/HTTP_Test/server/server-cert.pem";
    PrivateKeyPath = "/home/usr/Projects/HTTP_Test/server/server-key.pem";
    Ciphers = "HIGH:!MD5:!RC4:!aNULL:!eNULL";
    PreferServerCiphers = 1
  }
}
{
  ID = 1;
  Address = "195.218.228.2";
  Port = 8080;
}

[Client]
{
  ID = 0;
  DestAddress = { "192.168.205.146"; 8080 };
  Persistant = 1;
  ActivityTimer = 120;
  ResponseTimer = 60;
  MaxQueue = 5;
  MaxConnection = 50;
  HTTP_Version = 1;
  AdditionalDir = {
    ID = { 1; 2 };
    MaxErrorCount = 3;
    BreakDownTimeout = 50000;
    Necromancy = 1;
    UseLoadSharing = 1;
  }
}
{
  ID = 1;
  DestAddress = { "192.168.1.118"; 8881 };
  SrcAddress = "192.168.1.119";
  Persistant = 1;
  SSL = {
    Version = 4;
  }
  ActivityTimer = 90;
  ResponseTimer = 60;
  MaxQueue = 5;
  MaxConnection = 50;
}
{
  ID = 2;
  DestAddress = { "192.168.1.118"; 8882 };
  SrcAddress = "192.168.1.119";
  Persistant = 1;
}
----