---
title: long_mnc.cfg
description: Параметры PLMN с трехзначными MNC
type: docs
weight: 20
---

= long_mnc.cfg

ifeval::["{backend}" == "html5"]
include::partial$html5s.adoc[]
endif::[]

В файле задаются сети PLMN, имеющие трехзначные коды MNC. Каждая секция соответствует одной сети PLMN.

Ключ для перегрузки -- *reload long_mnc.cfg*.

.Описание параметров
[options="header",cols="4,8,2,1,1,2"]
|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| *[PLMN_list]*
| Перечень параметров сетей PLMN.
| [object]
| M
| R
| 1.46.0.0

| MCC
| Мобильный код страны.
| string
| M
| R
| 1.46.0.0

| MNC
| Код мобильной сети, состоящий из трех цифр.
| string
| M
| R
| 1.46.0.0
|===

.Пример
[source,textmate]
----
[PLMN_list]
{
	MCC = "404";
	MNC = "854";
}
{
	MCC = "404";
	MNC = "874";
}
----