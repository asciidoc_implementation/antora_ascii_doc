---
title: emergency_numbers.cfg
description: Параметры номеров экстренных служб
type: docs
weight: 20
---

= emergency_numbers.cfg

ifeval::["{backend}" == "html5"]
include::partial$html5s.adoc[]
endif::[]

В файле задаются связи между номерами экстренных служб и их типами. Имя секции может использоваться в качестве ссылки
в параметре xref:config/served_plmn.adoc#emergency_numbers[served_plmn.cfg::EmergencyNumbers].

Ключ для перегрузки -- *reload served_plmn.cfg*.

.Описание параметров
[options="header",cols="4,8,2,1,1,2"]
|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| Number
| Номер.
| string
| M
| R
|

| Police
| Флаг использования номера для полиции. +
По умолчанию: 0.
| bool
| O
| R
|

| Ambulance
| Флаг использования номера для cкорой помощи. +
По умолчанию: 0.
| bool
| O
| R
|

| FireBrigade
| Флаг использования номера для пожарной охраны. +
По умолчанию: 0.
| bool
| O
| R
|

| MarineGuard
| Флаг использования номера для береговой охраны. +
По умолчанию: 0.
| bool
| O
| R
|

| MountainRescue
| Флаг использования номера для горноспасательной службы. +
По умолчанию: 0.
| bool
| O
| R
|
|===

.Пример
[source,ini]
----
[112]
Number = "112";
Police = 1;
Ambulance = 1;
FireBrigade = 1;
MarineGuard = 1;
MountainRescue = 1;

[911]
Number = "911";
Police = 1;
Ambulance = 1;
FireBrigade = 1;
MarineGuard = 1;
MountainRescue = 1;

[AmbulanceOnly]
Number = "03";
Ambulance = 1;
----