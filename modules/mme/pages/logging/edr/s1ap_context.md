---
title: "S1AP Context EDR"
description: "Журнал s1ap_context_cdr"
weight: 20
type: docs
---

### Описание используемых полей ###

| N   | Поле                     | Описание                                                                                                                                                                                               | Тип      |
|-----|--------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|
| 1   | DateTime                 | Дата и время события. Формат:<br>`YYYY-MM-DD HH:MM:SS`.                                                                                                                                                | datetime |
| 2   | [Event](#event)          | Событие или процедура, сформировавшая запись.                                                                                                                                                          | string   |
| 3   | eNodeB UE ID             | Идентификатор S1-контекста на стороне eNodeB.                                                                                                                                                          | int      |
| 4   | MME UE ID                | Идентификатор S1-контекста на узле MME.                                                                                                                                                                | int      |
| 5   | IMSI                     | Номер IMSI абонента.                                                                                                                                                                                   | string   |
| 6   | MSISDN                   | Номер MSISDN абонента.                                                                                                                                                                                 | string   |
| 7   | GUTI                     | Глобальный уникальный временный идентификатор абонента.                                                                                                                                                | string   |
| 8   | IMEI                     | Номер IMEI устройства.                                                                                                                                                                                 | string   |
| 9   | PLMN                     | Идентификатор PLMN.                                                                                                                                                                                    | string   |
| 10  | TAC                      | Код области отслеживания.                                                                                                                                                                              | string   |
| 11  | CellID                   | Идентификатор соты.                                                                                                                                                                                    | int/hex  |
| 12  | [CauseType](#cause)      | Тип события.                                                                                                                                                                                           | string   |
| 13  | [CauseReason](#cause)    | Причина события.                                                                                                                                                                                       | string   |
| 14  | Duration                 | Длительность процедуры, в миллисекундах.                                                                                                                                                               | int      |
| 15  | Procedure Type:ErrorCode | Тип процедуры + Внутренний [код MME](../error_code/) результата. Формат:<br>`<procedure_type>:<error_code>`.<br>`1` — [Service Req](#service_req);<br>`2` — [S1 Context Release](#s1-context-release). | object   |

#### События и процедуры, Event {#event}

- [Service Req](#service_req);
- [S1 Context Release](#s1_context_release);
- [Reset](#reset).

#### Service Req {#service_req}

```log
DateTime,Service Req,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,PLMN,TAC,CellID,0,0,Duration,1:ErrorCode
```

Локальные коды ошибок

| Код | Описание                                                                      |
|-----|-------------------------------------------------------------------------------|
| 1   | Указана недопустимая сеть PLMN в запросе S1 Service Request.                  |
| 2   | У абонента нет подходящих bearer-служб.                                       |
| 3   | Запрошена неизвестная bearer-служба.                                          |
| 4   | Ошибка модификации bearer-службы.                                             |
| 5   | Отсутствует поле `User Data Container`.                                       |
| 6   | Не удалось декодировать значение ESM Message Container.                       |
| 7   | Не удалось сбросить контекст.                                                 |
| 8   | Для данного абонента неизвестен VLR GT.                                       |
| 9   | Ошибка процедуры S1AP: INITIAL CONTEXT SETUP.                                 |
| 10  | Получен повторный запрос S1 Service Request от абонента со смежного узла MME. |
| 11  | До завершения обработки текущего S1 Service Request был получен новый.        |

#### S1 Context Release {#s1_context_release}

```log
DateTime,S1 Context Release,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,PLMN,TAC,CellID,CauseType,CauseReason,Duration,2:ErrorCode
```

#### Reset {#reset}

```log
DateTime,Reset,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,PLMN,TAC,CellID,CauseType,CauseReason,Duration,3:ErrorCode
```

#### CauseType и CauseReason {#cause}

Поля **CauseType** и **CauseReason** описывают причину события S1AP.

**CauseType** задает группу событий. См. [3GPP TS 36.413](https://www.etsi.org/deliver/etsi_ts/136400_136499/136413/17.05.00_60/ts_136413v170500p.pdf).

Возможные значения:

* 1 — [Radio Network Layer cause](#radio_network_layer_cause), причины уровня радиосети;
* 2 — [Transport Layer cause](#transport_layer_cause), причины уровня передачи;
* 3 — [NAS cause](#nas_cause), причины уровня NAS;
* 4 — [Protocol cause](#protocol_cause), причины уровня протокола;
* 5 — [Miscellaneous cause](#miscellaneous_cause), остальные причины.

**CauseReason** задает порядковый номер причины события внутри группы **CauseType**. См. [3GPP TS 36.413](https://www.etsi.org/deliver/etsi_ts/136400_136499/136413/17.05.00_60/ts_136413v170500p.pdf).

##### Radio Network Layer cause {#radio_network_layer_cause}

| Код | Причина                                                         | Описание                                                                                                                                                                                                                                                                                                         |
|-----|-----------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 0   | Unspecified                                                     | Неописанная ошибка.                                                                                                                                                                                                                                                                                              |
| 1   | TX2RELOCOverall Expiry                                          | Таймер, контролирующий хэндовер X2, нештатно истек.                                                                                                                                                                                                                                                              |
| 2   | Successful Handover                                             | Хэндовер осуществлен успешно.                                                                                                                                                                                                                                                                                    |
| 3   | Release due to E-UTRAN generated reason                         | Причина отбоя вызвана сетью E-UTRAN.                                                                                                                                                                                                                                                                             |
| 4   | Handover Cancelled                                              | Вызвана отмена хэндовера.                                                                                                                                                                                                                                                                                        |
| 5   | Partial Handover                                                | Вызвана отмена хэндовера, поскольку в сообщении S1AP: HANDOVER COMMAND от MME задано поле E-RABs to Release List, и исходная eNodeB оценила, что непрерывность услуги будет лучше без хэндовера к указанному eNodeB назначения.                                                                                  |
| 6   | Handover Failure In Target EPC/eNB Or Target System             | Хэндовер завершился неуспешно ввиду ошибки EPC/eNodeB назначения.                                                                                                                                                                                                                                                |
| 7   | Handover Target not allowed                                     | Хэндовер в указанную соту назначения не разрешен.                                                                                                                                                                                                                                                                |
| 8   | TS1RELOCoverall Expiry                                          | Вызван истечением таймера TS1RELOCoverall.                                                                                                                                                                                                                                                                       |
| 9   | TS1RELOCprep Expiry                                             | Процедура Handover Preparation отменяется по истечении таймера TS1RELOCprep.                                                                                                                                                                                                                                     |
| 10  | Cell not available                                              | Соответствующая сота недоступна.                                                                                                                                                                                                                                                                                 |
| 11  | Unknown Target ID                                               | Хэндовер не разрешен, поскольку идентификатор назначения не известен EPC.                                                                                                                                                                                                                                        |
| 12  | No radio resources available in target cell                     | Нагрузка на соту назначения слишком высока.                                                                                                                                                                                                                                                                      |
| 13  | Unknown or already allocated MME UE S1AP ID                     | Запрошен, поскольку либо идентификатор MME UE S1AP ID неизвестен, либо (для первого сообщения, полученного на eNodeB) известен и привязан к уже существующему контексту.                                                                                                                                         |
| 14  | Unknown or already allocated eNB UE S1AP ID                     | Запрошен, поскольку либо идентификатор eNodeB UE S1AP ID неизвестен, либо (для первого сообщения, полученного узлом MME) известен и привязан к уже существующему контексту.                                                                                                                                      |
| 15  | Unknown or inconsistent pair of UE S1AP ID                      | Запрошен, поскольку либо идентификаторы UE S1AP ID неизвестны, либо известны, но связаны с различными контекстами.                                                                                                                                                                                               |
| 16  | Handover Desirable for Radio Reasons                            | Причина запроса хэндовера связана с радиосетью.                                                                                                                                                                                                                                                                  |
| 17  | Time Critical Handover                                          | Запрошен хэндовер по причине, которая критична по времени, т.е. значение зарезервировано для всех критических случаев, когда соединение наверняка будет потеряно при невыполнении хэндовера.                                                                                                                     |
| 18  | Resource Optimisation Handover                                  | Запрошен хэндовер для оптимизации распределения нагрузки на соседние соты.                                                                                                                                                                                                                                       |
| 19  | Reduce Load in Serving Cell                                     | Необходимо снизить нагрузку на обслуживающие соты. При подготовке к хэндоверу указывает на активацию хэндовера ввиду балансировки нагрузки.                                                                                                                                                                      |
| 20  | User Inactivity                                                 | Запрошен ввиду неактивности пользователя на всех E-<abbr title="Radio Access Bearer">RAB</abbr>, например, разрыв S1-соединения, для оптимизации использования ресурсов.                                                                                                                                         |
| 21  | Radio Connection With UE Lost                                   | Запрошен ввиду потери радиосоединения с UE.                                                                                                                                                                                                                                                                      |
| 22  | Load Balancing TAU Required                                     | Запрашивается для всех случаев балансировки и разгрузки узла MME.                                                                                                                                                                                                                                                |
| 23  | CS Fallback triggered                                           | Запрошен ввиду процедуры CS Fallback. Если находится в сообщении S1AP: UE CONTEXT RELEASE REQUEST, то указывает на отсутствие необходимости для EPC приостанавливать обслуживание в сети PS.                                                                                                                     |
| 24  | UE Not Available for PS Service                                 | Запрошен ввиду процедуры CS Fallback для сети GERAN. Если находится в сообщении S1AP: UE CONTEXT RELEASE REQUEST, то указывает на отсутствие необходимости для EPC приостанавливать обслуживание в сети PS ввиду отсутствия поддержки <abbr title="Dual Transfer Mode">DTM</abbr> GERAN-сотой назначения или UE. |
| 25  | Radio resources not available                                   | Запрашиваемые радиоресурсы недоступны.                                                                                                                                                                                                                                                                           |
| 26  | Invalid QoS combination                                         | Вызван некорректными значениями параметров QoS.                                                                                                                                                                                                                                                                  |
| 27  | Inter-RAT Redirection                                           | Запрошен ввиду переадресации между различными сетями RAT или LTE. Если находится в сообщении S1AP: UE CONTEXT RELEASE REQUEST, дальнейшие действия EPC должны следовать [3GPP TS 23.401](https://www.etsi.org/deliver/etsi_ts/123400_123499/123401/17.07.00_60/ts_123401v170700p.pdf).                           |
| 28  | Failure in the Radio Interface Procedure                        | Процедура Radio Interface Procedure завершилась неуспешно.                                                                                                                                                                                                                                                       |
| 29  | Interaction with other procedure                                | Вызван продолжающимся взаимодействием с другой процедурой.                                                                                                                                                                                                                                                       |
| 30  | Unknown E-RAB ID                                                | Вызван отсутствием идентификатора E-RAB на eNodeB.                                                                                                                                                                                                                                                               |
| 31  | Multiple E-RAB ID Instances                                     | Вызван передачей нескольких одинаковых E-RAB на eNodeB.                                                                                                                                                                                                                                                          |
| 32  | Encryption and/or integrity protection algorithms not supported | eNodeB не поддерживает ни один из алгоритмов шифрования/защиты целостности, которые поддерживаются UE.                                                                                                                                                                                                           |
| 33  | S1 Intra system Handover triggered                              | Вызван внутрисистемным хэндовером S1.                                                                                                                                                                                                                                                                            |
| 34  | S1 Inter system Handover triggered                              | Вызван межсистемным хэндовером S1.                                                                                                                                                                                                                                                                               |
| 35  | X2 Handover triggered                                           | Вызван хэндовером X2.                                                                                                                                                                                                                                                                                            |
| 36  | Redirection towards 1xRTT                                       | Запрошен разрыв логического, связанного с UE S1-соединения ввиду переадресации на 1xRTT-систему, например, CS fallback или SRVCC, когда необходима приостановка обслуживания в PS-сетях. Для процедуры сообщение может, но не обязано, содержать данные о переадресации.                                         |
| 37  | Not supported QCI Value                                         | Установление E-RAB завершилось ошибкой, поскольку указанный <abbr title="QoS Class Identifier">QCI</abbr> не поддерживается.                                                                                                                                                                                     |
| 38  | Invalid CSG Id                                                  | Идентификатор CSG, переданный eNodeB назначения, некорректен.                                                                                                                                                                                                                                                    |
| 39  | Release due to Pre-Emption                                      | Вызван приоритетным использованием службы.                                                                                                                                                                                                                                                                       |
| 40  | N26 interface not available                                     | Вызван временной ошибкой интерфейса N26.                                                                                                                                                                                                                                                                         |
| 41  | Insufficient UE Capabilities                                    | Вызван недостаточными возможностями UE.                                                                                                                                                                                                                                                                          |
| 42  | Maximum bearer pre-emption rate exceeded                        | Количество запросов превысило максимально допустимое для bearer-службы.                                                                                                                                                                                                                                          |
| 43  | UP integrity protection not possible                            | E-RAB не может быть принят ввиду политики обеспечения целостности плоскости пользователя.                                                                                                                                                                                                                        |


##### Transport Layer cause {#transport_layer_cause}

| Код | Причина                        | Описание                                                            |
|-----|--------------------------------|---------------------------------------------------------------------|
| 0   | Transport Resource Unavailable | Требуемые ресурсы для передачи недоступны.                          |
| 1   | Unspecified                    | Неописанная ошибка, принадлежащая к группе Transport Network Layer. |

##### NAS cause {#nas_cause}

| Код | Причина                 | Описание                                                                                                 |
|-----|-------------------------|----------------------------------------------------------------------------------------------------------|
| 0   | Normal Release          | Вызван нормальным сценарием.                                                                             |
| 1   | Authentication Failure  | Вызван неуспешной аутентификацией.                                                                       |
| 2   | Detach                  | Вызвано отключением от сети.                                                                             |
| 3   | Unspecified             | Неописанная ошибка, принадлежащая к группе NAS.                                                          |
| 4   | CSG Subscription Expiry | Вызвано тем, что UE перестает быть членом используемой <abbr title="Closed Subscriber Group">CSG</abbr>. |

##### Protocol cause {#protocol_cause}

| Код | Причина                                             | Описание                                                                                                           |
|-----|-----------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|
| 0   | Transfer Syntax Error                               | Полученное сообщение содержит синтаксическую ошибку передачи.                                                      |
| 1   | Abstract Syntax Error (Reject)                      | Полученное сообщение содержит абстрактную синтаксическую ошибку, чья критичность указывает на 'reject'.            |
| 2   | Abstract Syntax Error (Ignore And Notify)           | Полученное сообщение содержит абстрактную синтаксическую ошибку, чья критичность указывает на 'ignore and notify'. |
| 3   | Message Not Compatible With Receiver State          | Полученное сообщение не совместимо с текущим состоянием отправителя.                                               |
| 4   | Semantic Error                                      | Полученное сообщение содержит семантическую ошибку.                                                                |
| 5   | Abstract Syntax Error (Falsely Constructed Message) | Полученное сообщение содержит IE в неверном порядке или указанные слишком много раз.                               |
| 6   | Unspecified                                         | Неописанная ошибка, принадлежащая к группе Protocol cause.                                                         |

##### Miscellaneous cause {#miscellaneous_cause}

| Код | Причина                                              | Описание                                                                                                       |
|-----|------------------------------------------------------|----------------------------------------------------------------------------------------------------------------|
| 0   | Control Processing Overload                          | Перегрузка при обработке управлящего трафика.                                                                  |
| 1   | Not Enough User Plane Processing Resources Available | Недостаточно ресурсов для обработки пользовательского трафика.                                                 |
| 2   | Hardware Failure                                     | Вызван аппаратным сбоем.                                                                                       |
| 3   | O&M Intervention                                     | Вызван вмешательством OM.                                                                                      |
| 4   | Unspecified Failure                                  | Неописанная ошибка, не связанная с категориями Radio Network Layer, Transport Network Layer, NAS или Protocol. |
| 5   | Unknown PLMN                                         | Узел MME не обнаружил какую-либо сеть PLMN, предоставленную eNodeB.                                            |
