---
title: S1AP Overload
description: Журнал s1ap_overload_cdr
type: docs
weight: 20
---

= S1AP Overload

ifeval::["{backend}" == "html5"]
include::partial$html5s.adoc[]
endif::[]

.Описание используемых полей
[options="header",cols="1,4,8,2"]
|===
| N | Поле | Описание | Тип

| 1
| DateTime
| Дата и время события. Формат: +
*_YYYY-MM-DD HH:MM:SS_*.
| datetime

| 2
| Processed
| Количество обработанных сообщений.
| int

| 3
| Ignored
| Количество проигнорированных сообщений.
| int

| 4
| MessageType
| Тип сообщений. +
*_Initial UE Message_*/*_Attach Request_*.
| string
|===

[source,log]
----
DateTime,Processed,Ignored,MessageType
----