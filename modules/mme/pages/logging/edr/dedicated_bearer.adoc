---
title: Dedicated Bearer EDR
description: Журнал dedicated_bearer_cdr
type: docs
weight: 20
---

= Dedicated Bearer EDR

ifeval::["{backend}" == "html5"]
include::partial$html5s.adoc[]
endif::[]

.Описание используемых полей
[options="header",cols="1,4,8,2"]
|===
| N | Поле | Описание | Тип

| 1
| DateTime
| Дата и время события. Формат: +
*_YYYY-MM-DD HH:MM:SS_*.
| datetime

| 2
| <<event_dedicated_bearer,Event>>
| Событие или процедура, сформировавшая запись.
| string

| 3
| eNodeB UE ID
| Идентификатор S1-контекста на стороне исходной eNodeB.
| int

| 4
| MME UE ID
| Идентификатор S1-контекста на узле MME.
| int

| 5
| IMSI
| Номер IMSI абонента.
| string

| 6
| MSISDN
| Номер MSISDN абонента.
| string

| 7
| GUTI
| Глобальный уникальный временный идентификатор абонента.
| string

| 8
| IMEI
| Номер {IMEI} устройства.
| string

| 9
| PLMN
| Идентификатор {PLMN}.
| string

| 10
| TAC
| Код области отслеживания.
| string

| 11
| CellID
| Идентификатор соты.
| int/hex

| 12
| APN
| {APN}, относящееся к событию.
| string

| 13
| SGW IP
| IP-адрес узла SGW.
| ip

| 11
| SGW TEID
| Идентификатор конечной точки туннеля на узле SGW.
| string

| 12
| Self TEID
| Идентификатор конечной точки туннеля на узле MME.
| string

| 16
| Default Bearer ID
| Идентификатор bearer-службы по умолчанию для APN, относящемуся к событию.
| int

| 17
| Dedicated Bearer ID
| Идентификатор выделенной bearer-службы для APN, относящемуся к событию.
| int

| 18
| QCI
| Идентификатор класса QoS. См. https://www.etsi.org/deliver/etsi_ts/123200_123299/123203/16.02.00_60/ts_123203v160200p.pdf[3GPP TS 23.203]. +
NOTE: Для процедуры <<dedicated_bearer_deactivation,Dedicated Bearer Deactivation>> 0.
| int

| 19
| ESM Cause
| Причина завершения сессии EPS, EPS Session Management Cause. См. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf[3GPP TS 24.301]. +
NOTE: Для процедуры <<dedicated_bearer_activation,Dedicated Bearer Activation>> и <<dedicated_bearer_modification,Dedicated Bearer Modification>> 0.
| int

| 20
| Duration
| Длительность процедуры, в миллисекундах.
| int

| 21
| Procedure Type:ErrorCode
| Тип процедуры + Внутренний xref:logging/edr/error_code.adoc[код MME] результата. Формат: +
*_<procedure_type>:<error_code>_*. +
*_1_* -- <<dedicated_bearer_activation,Dedicated Bearer Activation>>; +
*_2_* -- <<dedicated_bearer_deactivation,Dedicated Bearer Deactivation>>; +
*_3_* -- <<dedicated_bearer_modification,Dedicated Bearer Modification>>.
| object
|===

[[event_dedicated_bearer]]
== События и процедуры, Event

* <<dedicated_bearer_activation,Dedicated Bearer Activation>>;
* <<dedicated_bearer_deactivation,Dedicated Bearer Deactivation>>;
* <<dedicated_bearer_modification,Dedicated Bearer Modification>>.

[[dedicated_bearer_activation]]
== Dedicated Bearer Activation

[source,log]
----
DateTime,Dedicated Bearer Activation,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,PLMN,TAC,CellID,APN,SGW IP,SGW TEID,Self TEID,Default Bearer ID,Dedicated Bearer ID,QCI,ESM Cause,Duration,1:ErrorCode
----

.Локальные коды ошибок
[options="header",cols="1,16"]
|===
| N | Описание

| 1
| Не осталось свободных идентификаторов для bearer-служб.

| 2
| Для абонента неизвестен *_MME UE ID_* и/или *_eNodeB UE ID_*.

| 3
| Для абонента неизвестны ключи шифрования.

| 4
| Не удалось декодировать сообщение GTP: Create Bearer Request.

| 5
| Для абонента неизвестен SGW {TEID}.

| 6
| В сообщении GTP: Create Bearer Request отсутствуют контексты bearer-служб.

| 7
| Запрос на создание bearer-службы отклонен eNodeB.

| 8
| Запрос на создание bearer-службы отклонен UE.

| 9
| Устройство абонента находится в режиме {PSM}.

| 10
| Запрос на создание выделенного bearer-службы для {NB-IoT} UE.

| 11
| Флаг {PPF} уже сброшен.

| 12
| Абонент недоступен для PS-сетей.
|===

[[dedicated_bearer_deactivation]]
== Dedicated Bearer Deactivation

[source,log]
----
DateTime,Dedicated Bearer Deactivation,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,PLMN,TAC,CellID,APN,SGW IP,SGW TEID,Self TEID,Default bearer ID,Dedicated bearer ID,0,ESM Cause,Duration,2:ErrorCode
----

.Локальные коды ошибок
[options="header",cols="1,16"]
|===
| N | Описание

| 1
| Нет данных о PDN-соединениях абонента.

| 2
| Ошибка сброса контекста.

| 3
| Устройство абонента находится в режиме {PSM}.

| 4
| Флаг {PPF} уже сброшен.

| 5
| Абонент недоступен для PS-сетей.

| 6
| Узел SGW отправил сообщение GTP: Failure Indication.

| 7
| Ошибка процедуры Paging.

| 8
| Запрос на деактивацию bearer-службы отклонен eNodeB.

| 9
| Указанная bearer-служба не найдена.
|===

[[dedicated_bearer_modification]]
== Dedicated Bearer Modification

[source,log]
----
DateTime,Dedicated Bearer Modification,eNodeB UE ID,MME UE ID,IMSI,MSISD,GUTI,IMEI,PLMN,TAC,CellID,APN,SGW IP,SGW TEID,Self TEID,Default bearer ID,Dedicated bearer ID,QCI,ESM Cause,Duration,3:ErrorCode
----

.Локальные коды ошибок
[options="header",cols="1,16"]
|===
| N | Описание

| 11
| Запрос Bearer Resource Modification Request отклонен eNodeB.

| 12
| Запрос Bearer Resource Modification Request отклонен UE.

| 13
| Запрос Bearer Resource Modification Request отклонен SGW.
|===