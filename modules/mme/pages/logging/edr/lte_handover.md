---
title: "LTE Handover EDR"
description: "Журнал lte_handover_cdr"
weight: 20
type: docs
---

### Описание используемых полей ###

| N   | Поле                     | Описание                                                                                                                                                                                 | Тип      |
|-----|--------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|
| 1   | DateTime                 | Дата и время события. Формат:<br>`YYYY-MM-DD HH:MM:SS`.                                                                                                                                  | datetime |
| 2   | [Event](#event)          | Событие или процедура, сформировавшая запись.                                                                                                                                            | string   |
| 3   | eNodeB UE ID             | Идентификатор S1-контекста на стороне исходной eNodeB.                                                                                                                                   | int      |
| 4   | Target eNodeB UE         | Идентификатор S1-контекста на стороне eNodeB назначения.                                                                                                                                 | int      |
| 5   | MME UE ID                | Идентификатор S1-контекста на узле MME.                                                                                                                                                  | int      |
| 6   | IMSI                     | Номер IMSI абонента.                                                                                                                                                                     | string   |
| 7   | MSISDN                   | Номер MSISDN абонента.                                                                                                                                                                   | string   |
| 8   | GUTI                     | Глобальный уникальный временный идентификатор абонента.                                                                                                                                  | string   |
| 9   | IMEI                     | Номер IMEI устройства.                                                                                                                                                                   | string   |
| 10  | Source PLMN              | Идентификатор исходной PLMN.                                                                                                                                                             | string   |
| 11  | Source TAC               | Код исходной области отслеживания.                                                                                                                                                       | string   |
| 12  | Source CellID            | Идентификатор исходной соты.                                                                                                                                                             | int/hex  |
| 13  | Source eNodeB IP         | IP-адрес исходной eNodeB.                                                                                                                                                                | ip       |
| 14  | Source IP SGW            | IP-адрес исходного узла SGW.                                                                                                                                                             | ip       |
| 15  | Target PLMN              | Идентификатор PLMN назначения.                                                                                                                                                           | string   |
| 16  | Target TAC               | Код области отслеживания назначения.                                                                                                                                                     | string   |
| 17  | Target CellID            | Идентификатор соты назначения.                                                                                                                                                           | int/hex  |
| 18  | Target eNodeB IP         | IP-адрес eNodeB назначения.                                                                                                                                                              | ip       |
| 19  | Target IP SGW            | IP-адрес узла SGW назначения.                                                                                                                                                            | ip       |
| 20  | Duration                 | Длительность процедуры, в миллисекундах.                                                                                                                                                 | int      |
| 21  | Procedure Type:ErrorCode | Тип процедуры + Внутренний [код MME](../error_code/) результата. Формат:<br>`<procedure_type>:<error_code>`.<br>`1` — [Handover S1](#handover_s1);<br>`2` — [Handover X2](#handover-x2). | object   |
| 22  | Target eNodeB ID         | Идентификатор eNodeB назначения.                                                                                                                                                         | int/hex  |

#### События и процедуры, Event {#event}

- [Handover S1](#handover_s1);
- [Handover X2](#handover_x2).

#### Handover S1 {#handover_s1}

```log
DateTime,S1,Source eNodeB UE ID,Target eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,Source PLMN,Source TAC,Source CellID,Source eNodeB IP,Source IP SGW,Target PLMN,Target TAC,Target CellID,Target eNodeB IP,Target IP SGW,Duration,1:ErrorCodeTarget eNodeB ID
```

Локальные коды ошибок:

| N   | Описание                                                                                                                                  |
|-----|-------------------------------------------------------------------------------------------------------------------------------------------|
| 1   | Недопустимый идентификатор PLMN назначения.                                                                                               |
| 2   | IP-адрес узла MME назначения неизвестен.                                                                                                  |
| 3   | Абонент в состоянии ECM-IDLE.                                                                                                             |
| 4   | Получен сообщение S1AP: HANDOVER FAILURE.                                                                                                 |
| 5   | Получено сообщение S1AP: HANDOVER CANCEL.                                                                                                 |
| 6   | MME назначения отказал в приёме абонента.                                                                                                 |
| 7   | SGW назначения отказал создание сессии.                                                                                                   |
| 8   | eNodeB назначения не приняла ни одной bearer-службы по умолчанию.                                                                         |
| 9   | eNodeB назначения не отправила запрос S1AP: HANDOVER REQUEST ACKNOWLEDGE.                                                                 |
| 10  | MME назначения не отправила запрос GTPv2-C: Forward Access Context Acknowledge.                                                           |
| 11  | eNodeB назначения не отправил сообщение S1AP: HANDOVER NOTIFY.                                                                            |
| 12  | MME назначения не отправил сообщение GTPv2-C: Forward Relocation Complete Notification.                                                   |
| 13  | eNodeB не отправила сообщение S1AP: UE CONTEXT RELEASE COMPLETE.                                                                          |
| 14  | Не удалось сбросить сессии на узле SGW.                                                                                                   |
| 15  | Попытка хэндовера из области <abbr title="Tracking Area Code">TAC</abbr> сети <abbr title="Narrow Band Internet of Things">NB-IoT</abbr>. |
| 16  | Ошибка обработки сообщения Context-Request.                                                                                               |

#### Handover X2 {#handover_x2}

```log
DateTime,X2,Source eNodeB UE ID,Target eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,Source PLMN,Source TAC,Source CellID,Source eNodeB IP,Source IP SGW,Target PLMN,Target TAC,Target CellID,Target eNodeB IP,Target IP SGW,Duration,2:ErrorCodeTarget eNodeB ID
```

Локальные коды ошибок:

| N   | Описание                                                 |
|-----|----------------------------------------------------------|
| 1   | Нечего создать на новом узле SGW.                        |
| 2   | Нечего активировать на предыдущем узле SGW.              |
| 3   | Не удалось ничего создать на новом узле SGW.             |
| 4   | Не удалось ничего модифицировать на предыдущем узле SGW. |
