---
title: S1AP EDR
description: Журнал s1ap_cdr
type: docs
weight: 20
---

= S1AP EDR

ifeval::["{backend}" == "html5"]
include::partial$html5s.adoc[]
endif::[]

.Описание используемых полей
[options="header",cols="1,4,8,2"]
|===
| N | Поле | Описание | Тип

| 1
| DateTime
| Дата и время события. Формат: +
*_YYYY-MM-DD HH:MM:SS_*.
| datetime

| 2
| <<event_s1ap,Event>>
| Событие или процедура, сформировавшая запись.
| string

| 3
| eNodeB UE ID
| Идентификатор S1-контекста на стороне eNodeB.
| int

| 4
| MME UE ID
| Идентификатор S1-контекста на узле MME.
| int

| 5
| IMSI
| Номер IMSI абонента.
| string

| 6
| MSISDN
| Номер MSISDN абонента.
| string

| 7
| GUTI
| Глобальный уникальный временный идентификатор абонента.
| string

| 8
| PLMN
| Идентификатор PLMN.
| string

| 9
| CellID
| Идентификатор соты.
| int/hex

| 10
| IMEI
| Номер IMEI устройства.
| string

| 11
| Duration
| Длительность процедуры, в миллисекундах.
| int

| 12
| ErrorCode
| Внутренний xref:logging/edr/error_code.adoc[код MME] результата.
| int
|===

[[event_s1ap]]
== События и процедуры, Event

* <<authentication,Authentication>>;
* <<bearer_resource_modification,Bearer Resource Modification>>;
* <<deactivate_bearer,Deactivate Bearer>>;
* <<e_rab_modification,E-RAB Modification>>;
* <<guti_reallocation,GUTI Reallocation>>;
* <<initial_context_setup,Initial Context Setup>>;
* <<mt_data_transport,MT Data Transport>>;
* <<mo_data_transport,MO Data Transport>>;
* <<modify_eps_bearer_context,Modify EPS bearer context>>;
* <<pdn_disconnection_s1ap,PDN Disconnection>>;
* <<secondary_rat_data_usage_report,Secondary RAT Data Usage Report>>;
* <<ue_context_modification,UE Context Modification>>;
* <<trace_failure_indication,Trace Failure Indication>>.

[[authentication]]
== Authentication

[source,log]
----
DateTime,Authentication,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
----

.Локальные коды ошибок
[options="header",cols="1,16"]
|===
| N | Описание

| 1
| Неуспешное сообщение Diameter: Authentication-Information-Answer.

| 2
| В сообщении Diameter: Authentication-Information-Answer отсутствуют данные для аутентификации.

| 3
| Полученный ключ {KASME} пуст.

| 4
| Получено сообщение S1 Security Mode Reject.

| 5
| Невозможно извлечь номер IMSI из сообщения Identity Response.

| 6
| Несовпадение полученного RES с ожидаемым XRES.

| 7
| Ошибка {MAC}.

| 8
| Не удалось определить алгоритм шифрования.

| 9
| Не удалось определить алгоритм контроля целостности.

| 10
| Недопустимое значение UE-Usage-Type.
|===

[[bearer_resource_modification]]
== Bearer Resource Modification

[source,log]
----
DateTime,Bearer Resource Modification,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
----

.Локальные коды ошибок
[options="header",cols="1,16"]
|===
| N | Описание

| 1
| Абонент находится в состоянии {EMM}-IDLE.

| 2
| Не указана ни одна bearer-служба.

| 3
| Запрошена неизвестная bearer-служба.

| 4
| Получен отказ от узла SGW.

| 5
| Ошибка вспомогательной логики.
|===

[[deactivate_bearer]]
== Deactivate Bearer

[source,log]
----
DateTime,Deactivate Bearer,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
----

.Локальные коды ошибок
[options="header",cols="1,16"]
|===
| N | Описание

| 7
| Ошибка процедуры Paging.

| 8
| Получен отказ от станции eNodeB.

| 9
| Указанная bearer-служба не найдена.
|===

[[e_rab_modification]]
== E-RAB Modification

[source,log]
----
DateTime,E-RAB Modification,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
----

.Локальные коды ошибок
[options="header",cols="1,16"]
|===
| N | Описание

| 1
| Не найдены подходящие для модификации bearer-службы.
|===

[[guti_reallocation]]
== GUTI Reallocation

[source,log]
----
DateTime,GUTI Reallocation,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
----

[[initial_context_setup]]
== Initial Context Setup

[source,log]
----
DateTime,Initial Context Setup,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,CauseType,CauseReason,Duration,ErrorCode
----

.Локальные коды ошибок
[options="header",cols="1,16"]
|===
| N | Описание

| 1
| Отсутствуют bearer-службы для активации.

| 2
| Получено сообщение ICS Failure.
|===

[[mt_data_transport]]
== MT Data Transport

[source,log]
----
DateTime,MT Data Transport,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
----

[[mo_data_transport]]
== MO Data Transport

[source,log]
----
DateTime,MO Data Transport,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
----

.Локальные коды ошибок
[options="header",cols="1,16"]
|===
| N | Описание

| 1
| Запрошена неизвестная bearer-служба.

| 2
| Ошибка модификации bearer-службы.

| 3
| Отсутствует значение User Data Container.
|===

[[modify_eps_bearer_context]]
== Modify EPS Bearer Context

[source,log]
----
DateTime,Modify EPS Bearer Context,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Bearer ID,Duration,ErrorCode
----

.Локальные коды ошибок
[options="header",cols="1,16"]
|===
| N | Описание

| 11
| Bearer-служба отвергнута eNodeB.

| 12
| Bearer-служба отвергнута UE.
|===

[[pdn_disconnection_s1ap]]
== PDN Disconnection

[source,log]
----
DateTime,PDN Disconnection,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,APN,IMEI,Duration,ErrorCode
----

.Локальные коды ошибок
[options="header",cols="1,16"]
|===
| N | Описание

| 1
| Не удалось декодировать сообщение PDN Disconnect Request.

| 2
| Не удалось извлечь связанную bearer-службу из сообщения PDN Disconnect Request.
|===

[[secondary_rat_data_usage_report]]
== Secondary RAT Data Usage Report

[source,log]
----
DateTime,Secondary RAT Data Usage Report,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
----

[[ue_context_modification]]
== UE Context Modification

[source,log]
----
DateTime,UE Context Modification,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
----

[[trace_failure_indication]]
== Trace Failure Indication

[source,log]
----
DateTime,Trace Failure Indication,ENB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,Cell ID,IMEI,Duration,ErrorCode
----