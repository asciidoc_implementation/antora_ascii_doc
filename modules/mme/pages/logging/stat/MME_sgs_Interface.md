---
title: "SGs interface"
description: "Статистика процедур SGs"
weight: 20
type: docs
---

Файл **<node_name>\_MME-sgsInterface_<datetime>_<granularity>.csv** содержит статистическую информацию по метрикам MME для интерфейса SGs.

### Описание параметров ###

Подробную информацию см. [3GPP TS 29.118](https://www.etsi.org/deliver/etsi_ts/129100_129199/129118/17.00.00_60/ts_129118v170000p.pdf).

| Tx/Rx | Метрика                                                             | Описание                                            | Группа    |
|-------|---------------------------------------------------------------------|-----------------------------------------------------|-----------|
| Tx    | <a name="sGsApLocationUpdateRequest">sGsApLocationUpdateRequest</a> | Количество сообщений SGsAP-LOCATION-UPDATE-REQUEST. | TAI:Value |
| Rx    | <a name="sGsApLocationUpdateAccept">sGsApLocationUpdateAccept</a>   | Количество сообщений SGsAP-LOCATION-UPDATE-ACCEPT.  | TAI:Value |

### Группа ###

| Название    | Описание                                                         | Тип |
|-------------|------------------------------------------------------------------|-----|
| TAI         | Идентификатор области отслеживания. Формат:<br>`<plmn_id><tac>`. | hex |
| \<plmn_id\> | Идентификатор сети PLMN.                                         | int |
| \<tac\>     | Код области отслеживания.                                        | hex |

#### Пример ####

```
TAI:2500103E8
```

#### Пример файла ####

```csv
tx,sGsApLocationUpdateRequest,,7
tx,sGsApLocationUpdateRequest,TAI:001010001,7
rx,sGsApLocationUpdateAccept,,7
rx,sGsApLocationUpdateAccept,TAI:001010001,7
```