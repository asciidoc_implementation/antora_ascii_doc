---
title: "Handover"
description: "Статистика процедур хэндовера"
weight: 20
type: docs
---

Файл **<node_name>\_MME-handover_<datetime>_<granularity>.csv** содержит статистическую информацию по метрикам MME для процедур хэндовера.

### Описание параметров ###

Подробную информацию см. [3GPP TS 29.060](https://www.etsi.org/deliver/etsi_ts/129000_129099/129060/17.04.00_60/ts_129060v170400p.pdf),  
[3GPP TS 29.274](https://www.etsi.org/deliver/etsi_ts/129200_129299/129274/17.09.00_60/ts_129274v170900p.pdf) и 
[3GPP TS 36.413](https://www.etsi.org/deliver/etsi_ts/136400_136499/136413/17.05.00_60/ts_136413v170500p.pdf).

| Tx/Rx | Метрика                                                                                       | Описание                                                                                                                                                                                                                                                                                                                                            | Группа |
|-------|-----------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|
| Rx    | <a name="interNodeHandoverRequestFromLteToUmts">interNodeHandoverRequestFromLteToUmts</a>     | Количество сообщений S1AP: HANDOVER REQUIRED, если целевой RNC находится вне зоны обслуживания узла во время процедуры inter-RAT Handover LTE-to-UMTS.<br>**Примечание.** Подсчитывает сообщения для следующих сценариев: LTE-to-UMTS Handover, Voice + Data SRVCC.                                                                                 |        |
| Rx    | <a name="interNodeHandoverSuccessFromLteToUmts">interNodeHandoverSuccessFromLteToUmts</a>     | Количество сообщений GTP-C: Forward Relocation Complete Notification или Forward Relocation Complete во время процедуры inter-RAT Handover LTE-to-UMTS.<br>**Примечание.** Подсчитывает сообщения для следующих сценариев: LTE-to-UMTS Handover, Voice + Data SRVCC, отправка сообщения S1AP: HANDOVER CANCEL от исходной eNodeB прежнему узлу MME. |        |
| Rx    | <a name="interNodeHandoverRequestFromUmtsToLte">interNodeHandoverRequestFromUmtsToLte</a>     | Количество сообщений GTP-C: Forward Relocation Request во время процедуры inter-RAT Handover UMTS-to-LTE.                                                                                                                                                                                                                                           |        |
| Rx    | <a name="interNodeHandoverSuccessFromUmtsToLte">interNodeHandoverSuccessFromUmtsToLte</a>     | Количество сообщений GTP-C: Forward Relocation Complete Acknowledge во время процедуры inter-RAT Handover UMTS-to-LTE.<br>**Примечание.** Подсчитывает сообщения для следующих сценариев: UMTS-to-LTE Handover, получение сообщения Relocation Cancel.                                                                                              |        |
| Rx    | <a name="interS1BasedHandoverRequestSgwNotChange">interS1BasedHandoverRequestSgwNotChange</a> | Количество сообщений S1AP: HANDOVER REQUIRED во время процедуры inter-RAT S1-based Handover, если UE не меняет SGW.                                                                                                                                                                                                                                 |        |
| Tx    | <a name="interS1BasedHandoverSuccessSgwNotChange">interS1BasedHandoverSuccessSgwNotChange</a> | Количество сообщений S1AP: HANDOVER COMMAND во время процедуры inter-RAT S1-based Handover, если UE не меняет SGW.                                                                                                                                                                                                                                  |        |
| Rx    | <a name="interS1BasedHandoverRequestSgwChange">interS1BasedHandoverRequestSgwChange</a>       | Количество сообщений S1AP: HANDOVER REQUIRED во время процедуры inter-RAT S1-based Handover, если UE перемещается в область другого узла SGW.                                                                                                                                                                                                       |        |
| Tx    | <a name="interS1BasedHandoverSuccessSgwChange">interS1BasedHandoverSuccessSgwChange</a>       | Количество сообщений S1AP: HANDOVER COMMAND во время процедуры inter-RAT S1-based Handover, если UE перемещается в область другого узла SGW.                                                                                                                                                                                                        |        |
| Rx    | <a name="intraS1BasedHandoverRequestSgwNotChange">intraS1BasedHandoverRequestSgwNotChange</a> | Количество сообщений S1AP: HANDOVER REQUIRED во время процедуры intra-RAT S1-based Handover, если UE не меняет SGW.                                                                                                                                                                                                                                 |        |
| Tx    | <a name="intraS1BasedHandoverSuccessSgwNotChange">intraS1BasedHandoverSuccessSgwNotChange</a> | Количество сообщений S1AP: HANDOVER COMMAND во время процедуры intra-RAT S1-based Handover, если UE не меняет SGW.                                                                                                                                                                                                                                  |        |
| Rx    | <a name="intraS1BasedHandoverRequestSgwChange">intraS1BasedHandoverRequestSgwChange</a>       | Количество сообщений S1AP: HANDOVER REQUIRED во время процедуры intra-RAT S1-based Handover, если UE перемещается в область другого узла SGW.                                                                                                                                                                                                       |        |
| Tx    | <a name="intraS1BasedHandoverSuccessSgwChange">intraS1BasedHandoverSuccessSgwChange</a>       | Количество сообщений S1AP: HANDOVER COMMAND во время процедуры intra-RAT S1-based Handover, если UE перемещается в область другого узла SGW.                                                                                                                                                                                                        |        |
| Rx    | <a name="intraX2BasedHandoverRequestSgwNotChange">intraX2BasedHandoverRequestSgwNotChange</a> | Количество сообщений S1AP: PATH SWITCH REQUEST во время процедуры intra-RAT X2-based Handover, если UE не меняет SGW.                                                                                                                                                                                                                               |        |
| Tx    | <a name="intraX2BasedHandoverSuccessSgwNotChange">intraX2BasedHandoverSuccessSgwNotChange</a> | Количество сообщений S1AP: PATH SWITCH REQUEST ACKNOWLEDGE во время процедуры intra-RAT X2-based Handover, если UE не меняет SGW.                                                                                                                                                                                                                   |        |
| Rx    | <a name="intraX2BasedHandoverRequestSgwChange">intraX2BasedHandoverRequestSgwChange</a>       | Количество сообщений S1AP: PATH SWITCH REQUEST во время процедуры intra-RAT X2-based Handover, если UE перемещается в область другого узла SGW.                                                                                                                                                                                                     |        |
| Tx    | <a name="intraX2BasedHandoverSuccessSgwChange">intraX2BasedHandoverSuccessSgwChange</a>       | Количество сообщений S1AP: PATH SWITCH REQUEST ACKNOWLEDGE во время процедуры intra-RAT X2-based Handover, если UE перемещается в область другого узла SGW.                                                                                                                                                                                         |        |

#### Пример файла ####

```csv
rx,interNodeHandoverSuccessFromLteToUmts,,0
rx,interNodeHandoverRequestFromLteToUmts,,0
rx,interNodeHandoverSuccessFromUmtsToLte,,0
rx,interNodeHandoverRequestFromUmtsToLte,,0
rx,interS1BasedHandoverRequestSgwNotChange,,0
rx,interS1BasedHandoverSuccessSgwNotChange,,0
rx,interS1BasedHandoverRequestSgwChange,,0
rx,interS1BasedHandoverSuccessSgwChange,,0
rx,intraS1BasedHandoverRequestSgwNotChange,,0
rx,intraS1BasedHandoverSuccessSgwNotChange,,0
rx,intraS1BasedHandoverRequestSgwChange,,0
rx,intraS1BasedHandoverSuccessSgwChange,,0
rx,intraX2BasedHandoverRequestSgwNotChange,,7
tx,intraX2BasedHandoverSuccessSgwNotChange,,7
rx,intraX2BasedHandoverRequestSgwChange,,0
rx,intraX2BasedHandoverSuccessSgwChange,,0
```