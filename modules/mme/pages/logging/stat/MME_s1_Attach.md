---
title: "S1 Attach"
description: "Статистика процедур S1 Attach"
weight: 20
type: docs
---

Файл **<node_name>\_MME-s1Attach_<datetime>_<granularity>.csv** содержит статистическую информацию по метрикам MME для интерфейса S1 по процедурам Attach.

### Описание параметров ###

Подробную информацию см. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).

| Tx/Rx | Метрика                                                     | Описание                                                                                                            | Группа                    |
|-------|-------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------|---------------------------|
| Rx    | <a name="attachRequest">attachRequest</a>                   | Количество сообщений S1 Attach Request с `EPS Attach Type = EPS attach`.                                            | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachSuccess">attachSuccess</a>                   | Количество сообщений S1 Attach Accept с `EPS Attach Type = EPS attach`.                                             | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail">attachFail</a>                         | Количество сообщений S1 Attach Reject.                                                                              | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail2">attachFail2</a>                       | Количество сообщений S1 Attach Reject с причиной "#2 IMSI unknown in HSS".                                          | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail3">attachFail3</a>                       | Количество сообщений S1 Attach Reject с причиной "#3 Illegal UE".                                                   | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail6">attachFail6</a>                       | Количество сообщений S1 Attach Reject с причиной "#6 Illegal ME".                                                   | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail7">attachFail7</a>                       | Количество сообщений S1 Attach Reject с причиной "#7 EPS services not allowed".                                     | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail8">attachFail8</a>                       | Количество сообщений S1 Attach Reject с причиной "#8 EPS and non-EPS services not allowed".                         | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail11">attachFail11</a>                     | Количество сообщений S1 Attach Reject с причиной "#11 PLMN not allowed".                                            | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail12">attachFail12</a>                     | Количество сообщений S1 Attach Reject с причиной "#12 Tracking Area not allowed".                                   | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail13">attachFail13</a>                     | Количество сообщений S1 Attach Reject с причиной "#13 Roaming not allowed in this tracking area".                   | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail14">attachFail14</a>                     | Количество сообщений S1 Attach Reject с причиной "#14 EPS services not allowed in this PLMN".                       | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail15">attachFail15</a>                     | Количество сообщений S1 Attach Reject с причиной "#15 No Suitable Cells In tracking area".                          | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail17">attachFail17</a>                     | Количество сообщений S1 Attach Reject с причиной "#17 Network failure".                                             | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail19">attachFail19</a>                     | Количество сообщений S1 Attach Reject с причиной "#19 ESM failure".                                                 | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail25">attachFail25/a>                      | Количество сообщений S1 Attach Reject с причиной "#25 Not authorized for this CSG".                                 | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail111">attachFail111</a>                   | Количество сообщений S1 Attach Reject с причиной "#111 Protocol error, unspecified".                                | TAI:Value, IMSIPLMN:Value |
| Rx    | <a name="combinedAttachRequest">combinedAttachRequest</a>   | Количество сообщений S1 Attach Request с `EPS Attach type = combined EPS/IMSI attach`.                              | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachSuccess">combinedAttachSuccess</a>   | Количество сообщений S1 Attach Accept с `EPS Attach type = combined EPS/IMSI attach`.                               | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail">combinedAttachFail</a>         | Количество сообщений S1 Attach Reject в ответ на  Attach Request с IE `EPS Attach type = combined EPS/IMSI attach`. | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail3">combinedAttachFail3</a>       | Количество сообщений S1 Attach Reject с причиной "#3 Illegal UE".                                                   | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail6">combinedAttachFail6</a>       | Количество сообщений S1 Attach Reject с причиной "#6 Illegal ME".                                                   | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail7">combinedAttachFail7</a>       | Количество сообщений S1 Attach Reject с причиной "#7 EPS services not allowed".                                     | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail8">combinedAttachFail8</a>       | Количество сообщений S1 Attach Reject с причиной "#8 EPS and non-EPS services not allowed".                         | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail11">combinedAttachFail11</a>     | Количество сообщений S1 Attach Reject с причиной "#11 PLMN not allowed".                                            | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail12">combinedAttachFail12</a>     | Количество сообщений S1 Attach Reject с причиной "#12 Tracking Area not allowed".                                   | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail13">combinedAttachFail13</a>     | Количество сообщений S1 Attach Reject с причиной "#13 Roaming not allowed in this tracking area".                   | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail14">combinedAttachFail14</a>     | Количество сообщений S1 Attach Reject с причиной "#14 EPS services not allowed in this PLMN".                       | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail15">combinedAttachFail15</a>     | Количество сообщений S1 Attach Reject с причиной "#15 No Suitable Cells In tracking area".                          | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail19">combinedAttachFail19</a>     | Количество сообщений S1 Attach Reject с причиной "#19 ESM failure".                                                 | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail25">combinedAttachFail25</a>     | Количество сообщений S1 Attach Reject с причиной "#25 Not authorized for this CSG".                                 | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail111">combinedAttachFail111</a>   | Количество сообщений S1 Attach Reject с причиной "#111 Protocol error, unspecified".                                | TAI:Value, IMSIPLMN:Value |
| Rx    | <a name="emergencyAttachRequest">emergencyAttachRequest</a> | Количество сообщений S1 Attach Request с `EPS Attach type = EPS emergency attach`.                                  | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="emergencyAttachSuccess">emergencyAttachSuccess</a> | Количество сообщений S1 Attach Accept с `EPS Attach type = EPS emergency attach`.                                   | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="emergencyAttachFail">emergencyAttachFail</a>       | Количество сообщений S1 Attach Reject с `EPS Attach type = EPS emergency attach`.                                   | TAI:Value, IMSIPLMN:Value |

**Примечание.** Описание причин ошибок EMM см. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf).

### Группа ###

| Название    | Описание                                                         | Тип |
|-------------|------------------------------------------------------------------|-----|
| TAI         | Идентификатор области отслеживания. Формат:<br>`<plmn_id><tac>`. | hex |
| \<plmn_id\> | Идентификатор сети PLMN.                                         | int |
| \<tac\>     | Код области отслеживания.                                        | hex |
| IMSIPLMN    | Идентификатор сети PLMN на основе первых 5 цифр из номера IMSI.  | int |

#### Пример ####

```
TAI:2500103E8
IMSIPLMN:25099
```

#### Пример файла ####

```csv
rx,attachRequest,,288
rx,attachRequest,IMSIPLMN:20893,287
rx,attachRequest,TAI:208930001,288
tx,attachSuccess,,276
tx,attachSuccess,IMSIPLMN:20893,276
tx,attachSuccess,TAI:208930001,276
tx,attachFail17,,15
tx,attachFail17,IMSIPLMN:20893,14
tx,attachFail17,TAI:208930001,15
rx,combinedAttachRequest,,7
rx,combinedAttachRequest,IMSIPLMN:00101,2
rx,combinedAttachRequest,IMSIPLMN:25020,1
rx,combinedAttachRequest,IMSIPLMN:25060,2
rx,combinedAttachRequest,IMSIPLMN:99999,2
rx,combinedAttachRequest,TAI:001010001,7
tx,combinedAttachSuccess,,5
tx,combinedAttachSuccess,IMSIPLMN:00101,2
tx,combinedAttachSuccess,IMSIPLMN:99999,3
tx,combinedAttachSuccess,TAI:001010001,4
tx,combinedAttachSuccess,TAI:999990001,1
tx,combinedAttachFail12,,3
tx,combinedAttachFail12,IMSIPLMN:25020,1
tx,combinedAttachFail12,IMSIPLMN:25060,2
tx,combinedAttachFail12,TAI:001010001,3
rx,emergencyAttachRequest,,0
tx,emergencyAttachSuccess,,0
```