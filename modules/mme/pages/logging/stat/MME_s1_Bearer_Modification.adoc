---
title: S1 Bearer Modification
description: Статистика процедур S1 Bearer Modification
type: docs
weight: 20
---

= S1 Bearer Modification

ifeval::["{backend}" == "html5"]
include::partial$html5s.adoc[]
endif::[]

Файл **<node_name>\_MME-s1BearerModification_<datetime>_<granularity>.csv** содержит статистическую информацию по метрикам MME для процедур S1 Bearer Modification.

Подробную информацию см. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf[3GPP TS 24.301].

.Описание параметров
[options="header",cols="2,6,8,4"]
|===
| Tx/Rx | Метрика | Описание | Группа

| Tx
| [[pgwInitBearerModRequest]]pgwInitBearerModRequest
| Количество сообщений S1 Modify EPS Bearer Context Request в процедурах PGW Initiated Bearer Modification (PGW_INIT_BEARER_MOD).
| IMSIPLMN:Value

| Rx
| [[pgwInitBearerModSuccess]]pgwInitBearerModSuccess
| Количество сообщений S1 Modify EPS Bearer Context Accept в процедурах PGW Initiated Bearer Modification (PGW_INIT_BEARER_MOD).
| IMSIPLMN:Value

| Tx
| [[hssInitBearerModRequest]]hssInitBearerModRequest
| Количество сообщений S1 Modify EPS Bearer Context Request в процедурах HSS Initiated Subscribed QoS Modification (HSS_INIT_SUBSCRIBED_QOS_MOD).
| IMSIPLMN:Value

| Rx
| [[hssInitBearerModSuccess]]hssInitBearerModSuccess
| Количество сообщений S1 Modify EPS Bearer Context Accept в процедурах HSS Initiated Subscribed QoS Modification (HSS_INIT_SUBSCRIBED_QOS_MOD).
| IMSIPLMN:Value

| Rx
| [[modifyEpsBearerContextReject]]modifyEpsBearerContextReject
| Количество сообщений S1 Modify EPS Bearer Context Reject.
| IMSIPLMN:Value

| Rx
| [[modifyEpsBearerContextReject26]]modifyEpsBearerContextReject26
| Количество сообщений S1 Modify EPS Bearer Context Reject с причиной "#26 Insufficient resources".
| IMSIPLMN:Value

| Rx
| [[modifyEpsBearerContextReject111]]modifyEpsBearerContextReject111
| Количество сообщений S1 Modify EPS Bearer Context Reject с причиной "#111 Protocol error, unspecified".
| IMSIPLMN:Value

| Rx
| [[ueInitBearerResModRequest]]ueInitBearerResModRequest
| Количество сообщений S1 Bearer Resource Modification Request.
| IMSIPLMN:Value

| Tx
| [[ueInitBearerResModReject]]ueInitBearerResModReject
| Количество сообщений S1 Bearer Resource Modification Reject.
| IMSIPLMN:Value

| Tx
| [[ueInitBearerResModReject26]]ueInitBearerResModReject26
| Количество сообщений S1 Bearer Resource Modification Reject с причиной "#26 Insufficient resources".
| IMSIPLMN:Value

| Tx
| [[ueInitBearerResModReject30]]ueInitBearerResModReject30
| Количество сообщений S1 Bearer Resource Modification Reject с причиной "#30 Request rejected by Serving GW or PDN GW".
| IMSIPLMN:Value

| Tx
| [[ueInitBearerResModReject31]]ueInitBearerResModReject31
| Количество сообщений S1 Bearer Resource Modification Reject с причиной "#31 Request rejected, unspecified".
| IMSIPLMN:Value

| Tx
| [[ueInitBearerResModReject32]]ueInitBearerResModReject32
| Количество сообщений S1 Bearer Resource Modification Reject с причиной "#32 Service option not supported".
| IMSIPLMN:Value

| Tx
| [[ueInitBearerResModReject33]]ueInitBearerResModReject33
| Количество сообщений S1 Bearer Resource Modification Reject с причиной "#33 Requested service option not subscribed".
| IMSIPLMN:Value

| Tx
| [[ueInitBearerResModReject37]]ueInitBearerResModReject37
| Количество сообщений S1 Bearer Resource Modification Reject с причиной "#37 EPS QoS not accepted".
| IMSIPLMN:Value

| Tx
| [[ueInitBearerResModReject59]]ueInitBearerResModReject59
| Количество сообщений S1 Bearer Resource Modification Reject с причиной "#59 Unsupported QCI value".
| IMSIPLMN:Value

| Tx
| [[ueInitBearerResModReject96]]ueInitBearerResModReject96
| Количество сообщений S1 Bearer Resource Modification Reject с причиной "#96 Invalid mandatory information".
| IMSIPLMN:Value

| Tx
| [[ueInitBearerResModReject111]]ueInitBearerResModReject111
| Количество сообщений S1 Bearer Resource Modification Reject с причиной "#111 Protocol error, unspecified".
| IMSIPLMN:Value
|===

NOTE: Описание причин ошибок ESM см. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf[3GPP TS 24.301].

.Группа
[cols="1,6,2"]

|===
| Название | Описание | Тип

| IMSIPLMN
| Идентификатор сети PLMN на основе первых 5 цифр из номера IMSI.
| int
|===

.Пример
[source,console]
----
IMSIPLMN:25099
----

.Пример
[source,csv]
----
tx,pgwInitBearerModRequest,,0
rx,pgwInitBearerModSuccess,,0
tx,hssInitBearerModRequest,,0
rx,hssInitBearerModSuccess,,0
rx,modifyEpsBearerContextReject,,0
rx,ueInitBearerResModRequest,,0
tx,ueInitBearerResModReject,,0
----