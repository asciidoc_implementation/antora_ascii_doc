---
title: S1 Bearer Deactivation
description: Статистика процедур S1 Bearer Deactivation
type: docs
weight: 20
---

= S1 Bearer Deactivation

ifeval::["{backend}" == "html5"]
include::partial$html5s.adoc[]
endif::[]

Файл **<node_name>\_MME-s1BearerDeactivation_<datetime>_<granularity>.csv** содержит статистическую информацию по метрикам MME для процедур S1 Bearer Deactivation.

Подробную информацию см. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf[3GPP TS 24.301].

.Описание параметров
[options="header",cols="2,6,8,4"]
|===
| Tx/Rx | Метрика | Описание | Группа

| Tx
| [[deactivateEpsBearerContextRequest]]deactivateEpsBearerContextRequest
| Количество сообщений S1 Deactivate EPS Bearer Context Request.
| IMSIPLMN:Value

| Rx
| [[deactivateEpsBearerContextAccept]]deactivateEpsBearerContextAccept
| Количество сообщений S1 Deactivate EPS Bearer Context Accept.
| IMSIPLMN:Value

| Tx
| [[defaultBearerDeactivationRequest]]defaultBearerDeactivationRequest
| Количество сообщений S1 Deactivate EPS Bearer Context Request с *_EBI = 5_*.
| IMSIPLMN:Value

| Tx
| [[defaultBearerDeactivationRequest26]]defaultBearerDeactivationRequest26
| Количество сообщений S1 Deactivate EPS Bearer Context Request с *_EBI = 5_* и причиной "#26 Insufficient resources".
| IMSIPLMN:Value

| Tx
| [[defaultBearerDeactivationRequest36]]defaultBearerDeactivationRequest36
| Количество сообщений S1 Deactivate EPS Bearer Context Request с *_EBI = 5_* и причиной "#36 Regular deactivation".
| IMSIPLMN:Value

| Tx
| [[defaultBearerDeactivationRequest38]]defaultBearerDeactivationRequest38
| Количество сообщений S1 Deactivate EPS Bearer Context Request с *_EBI = 5_* и причиной "#38 Network failure".
| IMSIPLMN:Value

| Tx
| [[defaultBearerDeactivationRequest39]]defaultBearerDeactivationRequest39
| Количество сообщений S1 Deactivate EPS Bearer Context Request с *_EBI = 5_* и причиной "#39 Reactivation requested".
| IMSIPLMN:Value

| Tx
| [[dedicatedBearerDeactivationRequest]]dedicatedBearerDeactivationRequest
| Количество сообщений S1 Deactivate EPS Bearer Context Request с заполненным полем *_Dedicated Bearer ID_*.
| IMSIPLMN:Value

| Tx
| [[dedicatedBearerDeactivationRequest26]]dedicatedBearerDeactivationRequest26
| Количество сообщений S1 Deactivate EPS Bearer Context Request с заполненным полем *_Dedicated Bearer ID_* и причиной "#26 Insufficient resources".
| IMSIPLMN:Value

| Tx
| [[dedicatedBearerDeactivationRequest36]]dedicatedBearerDeactivationRequest36
| Количество сообщений S1 Deactivate EPS Bearer Context Request с заполненным полем *_Dedicated Bearer ID_* и причиной "#36 Regular deactivation".
| IMSIPLMN:Value

| Tx
| [[dedicatedBearerDeactivationRequest38]]dedicatedBearerDeactivationRequest38
| Количество сообщений S1 Deactivate EPS Bearer Context Request с заполненным полем *_Dedicated Bearer ID_* и причиной "#38 Network failure".
| IMSIPLMN:Value

| Tx
| [[dedicatedBearerDeactivationRequest39]]dedicatedBearerDeactivationRequest39
| Количество сообщений S1 Deactivate EPS Bearer Context Request с заполненным полем *_Dedicated Bearer ID_* и причиной "#39 Reactivation requested".
| IMSIPLMN:Value

| Rx
| [[pdnDisconnectRequest]]pdnDisconnectRequest
| Количество сообщений S1 PDN Disconnect Request.
| IMSIPLMN:Value

| Tx
| [[pdnDisconnectSuccess]]pdnDisconnectSuccess
| Количество сообщений S1 Deactivate EPS Bearer Context Request в ответ на запрос S1 PDN Disconnect Request.
| IMSIPLMN:Value

| Tx
| [[pdnDisconnectReject]]pdnDisconnectReject
| Количество сообщений S1 PDN Disconnect Reject.
| IMSIPLMN:Value

| Tx
| [[pdnDisconnectReject35]]pdnDisconnectReject35
| Количество сообщений S1 PDN Disconnect Reject с причиной "#35 PTI already in use".
| IMSIPLMN:Value

| Tx
| [[pdnDisconnectReject43]]pdnDisconnectReject43
| Количество сообщений S1 PDN Disconnect Reject с причиной "#43 Invalid EPS bearer identity".
| IMSIPLMN:Value

| Tx
| [[pdnDisconnectReject49]]pdnDisconnectReject49
| Количество сообщений S1 PDN Disconnect Reject с причиной "#49 Last PDN disconnection not allowed".
| IMSIPLMN:Value

| Tx
| [[pdnDisconnectReject96]]pdnDisconnectReject96
| Количество сообщений S1 PDN Disconnect Reject с причиной "#96 Invalid mandatory information".
| IMSIPLMN:Value

| Tx
| [[pdnDisconnectReject111]]pdnDisconnectReject111
| Количество сообщений S1 PDN Disconnect Reject с причиной "#111 Protocol error, unspecified".
| IMSIPLMN:Value
|===

NOTE: Описание причин ошибок ESM см. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf[3GPP TS 24.301].

.Группа
[cols="1,6,2"]

|===
| Название | Описание | Тип

| IMSIPLMN
| Идентификатор сети PLMN на основе первых 5 цифр из номера IMSI.
| int
|===

.Пример
[source,console]
----
IMSIPLMN,25099
----

.Пример
[source,csv]
----
tx,deactivateEpsBearerContextRequest,,0
rx,deactivateEpsBearerContextAccept,,0
tx,defaultBearerDeactivationRequest,,0
tx,dedicatedBearerDeactivationRequest,,0
rx,pdnDisconnectRequest,,0
tx,pdnDisconnectReject,,0
----