---
title: "Tracking Area Update"
description: "Статистика процедур TAU"
weight: 20
type: docs
---

Файл **<node_name>\_MME-tau_<datetime>_<granularity>.csv** содержит статистическую информацию по метрикам MME для процедур TRACKING AREA UPDATE.

### Описание параметров ###

Подробную информацию см. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).

| Tx/Rx | Метрика                                                                     | Описание                                                                                                                                                                | Группа    |
|-------|-----------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| Rx    | <a name="intraTauRequest">intraTauRequest</a>                               | Количество сообщений NAS TRACKING AREA UPDATE REQUEST c `Update Type = TA updating`.                                                                                    | TAI:Value |
| Tx    | <a name="intraTauSuccess">intraTauSuccess</a>                               | Количество сообщений NAS TRACKING AREA UPDATE ACCEPT с `Update Type = TA updating`.                                                                                     | TAI:Value |
| Tx    | <a name="intraTauReject">intraTauReject</a>                                 | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating`.                                                                                     |           |
| Tx    | <a name="intraTauReject3">intraTauReject3</a>                               | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#3 Illegal UE".                                                          | TAI:Value |
| Tx    | <a name="intraTauReject6">intraTauReject6</a>                               | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#6 Illegal ME".                                                          | TAI:Value |
| Tx    | <a name="intraTauReject7">intraTauReject7</a>                               | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#7 EPS services not allowed".                                            | TAI:Value |
| Tx    | <a name="intraTauReject9">intraTauReject9</a>                               | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#9 UE identity cannot be derived by the network".                        | TAI:Value |
| Tx    | <a name="intraTauReject10">intraTauReject10</a>                             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#10 Implicitly detached".                                                | TAI:Value |
| Tx    | <a name="intraTauReject11">intraTauReject11</a>                             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#11 PLMN not allowed".                                                   | TAI:Value |
| Tx    | <a name="intraTauReject12">intraTauReject12</a>                             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#12 Tracking Area not allowed".                                          | TAI:Value |
| Tx    | <a name="intraTauReject13">intraTauReject13</a>                             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#13 Roaming not allowed in this tracking area".                          | TAI:Value |
| Tx    | <a name="intraTauReject14">intraTauReject14</a>                             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#14 EPS services not allowed in this PLMN".                              | TAI:Value |
| Tx    | <a name="intraTauReject15">intraTauReject15</a>                             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#15 No Suitable Cells In tracking area".                                 | TAI:Value |
| Tx    | <a name="intraTauReject40">intraTauReject40</a>                             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#40 No EPS bearer context activated".                                    | TAI:Value |
| Tx    | <a name="intraTauReject111">intraTauReject111</a>                           | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#111 Protocol error, unspecified".                                       | TAI:Value |
| Rx    | <a name="periodTauRequest">periodTauRequest</a>                             | Количество сообщений NAS TRACKING AREA UPDATE REQUEST с `Update Type = periodic updating`.                                                                              | TAI:Value |
| Tx    | <a name="periodTauSuccess">periodTauSuccess</a>                             | Количество сообщений NAS TRACKING AREA UPDATE ACCEPT с `Update Type = periodic updating`.                                                                               | TAI:Value |
| Tx    | <a name="periodTauReject">periodTauReject</a>                               | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = periodic updating`.                                                                               |           |
| Tx    | <a name="periodTauReject3">periodTauReject3</a>                             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = periodic updating` и причиной "#3 Illegal UE".                                                    | TAI:Value |
| Tx    | <a name="periodTauReject6">periodTauReject6</a>                             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = periodic updating` и причиной "#6 Illegal ME".                                                    | TAI:Value |
| Tx    | <a name="periodTauReject7">periodTauReject7</a>                             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = periodic updating` и причиной "#7 EPS services not allowed".                                      | TAI:Value |
| Tx    | <a name="periodTauReject9">periodTauReject9</a>                             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = periodic updating` и причиной "#9 UE identity cannot be derived by the network".                  | TAI:Value |
| Tx    | <a name="periodTauReject10">periodTauReject10</a>                           | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = periodic updating` и причиной "#10 Implicitly detached".                                          | TAI:Value |
| Tx    | <a name="periodTauReject11">periodTauReject11</a>                           | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = periodic updating` и причиной "#11 PLMN not allowed".                                             | TAI:Value |
| Tx    | <a name="periodTauReject12">periodTauReject12</a>                           | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = periodic updating` и причиной "#12 Tracking Area not allowed".                                    | TAI:Value |
| Tx    | <a name="periodTauReject13">periodTauReject13</a>                           | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = periodic updating` и причиной "#13 Roaming not allowed in this tracking area".                    | TAI:Value |
| Tx    | <a name="periodTauReject14">periodTauReject14</a>                           | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = periodic updating` и причиной "#14 EPS services not allowed in this PLMN".                        | TAI:Value |
| Tx    | <a name="periodTauReject15">periodTauReject15</a>                           | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = periodic updating` и причиной "#15 No Suitable Cells In tracking area".                           | TAI:Value |
| Tx    | <a name="periodTauReject40">periodTauReject40</a>                           | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = periodic updating` и причиной "#40 No EPS bearer context activated".                              | TAI:Value |
| Tx    | <a name="periodTauReject111">periodTauReject111</a>                         | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = periodic updating` и причиной "#111 Protocol error, unspecified".                                 | TAI:Value |
| Rx    | <a name="InterTauRequest">InterTauRequest</a>                               | Количество сообщений inter-MME NAS TRACKING AREA UPDATE REQUEST c `Update Type = TA updating`.                                                                          | TAI:Value |
| Tx    | <a name="interTauSuccess">interTauSuccess</a>                               | Количество сообщений inter-MME NAS TRACKING AREA UPDATE ACCEPT с `Update Type = TA updating`.                                                                           | TAI:Value |
| Tx    | <a name="interTauReject">interTauReject</a>                                 | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating`.                                                                                     | TAI:Value |
| Tx    | <a name="interTauReject3">interTauReject3</a>                               | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#3 Illegal UE".                                                          | TAI:Value |
| Tx    | <a name="interTauReject6">interTauReject6</a>                               | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#6 Illegal ME".                                                          | TAI:Value |
| Tx    | <a name="interTauReject7">interTauReject7</a>                               | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#7 EPS services not allowed".                                            | TAI:Value |
| Tx    | <a name="interTauReject9">interTauReject9</a>                               | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#9 UE identity cannot be derived by the network".                        | TAI:Value |
| Tx    | <a name="interTauReject10">interTauReject10</a>                             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#10 Implicitly detached".                                                | TAI:Value |
| Tx    | <a name="interTauReject11">interTauReject11</a>                             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#11 PLMN not allowed".                                                   | TAI:Value |
| Tx    | <a name="interTauReject12">interTauReject12</a>                             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#12 Tracking Area not allowed".                                          | TAI:Value |
| Tx    | <a name="interTauReject13">interTauReject13</a>                             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#13 Roaming not allowed in this tracking area".                          | TAI:Value |
| Tx    | <a name="interTauReject14">interTauReject14</a>                             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#14 EPS services not allowed in this PLMN".                              | TAI:Value |
| Tx    | <a name="interTauReject15">interTauReject15</a>                             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#15 No Suitable Cells In tracking area".                                 | TAI:Value |
| Tx    | <a name="interTauReject40">interTauReject40</a>                             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#40 No EPS bearer context activated".                                    | TAI:Value |
| Tx    | <a name="interTauReject111">interTauReject111r</a>                          | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = TA updating` и причиной "#111 Protocol error, unspecified".                                       | TAI:Value |
| Rx    | <a name="intraCombinedTauRequest">intraCombinedTauRequest</a>               | Количество сообщений NAS TRACKING AREA UPDATE REQUEST c `Update Type = combined TA/LA updating` или `Update Type = combined TA/LA updating with IMSI attach`.           | TAI:Value |
| Tx    | <a name="intraCombinedTauSuccess">intraCombinedTauSuccess</a>               | Количество сообщений NAS TRACKING AREA UPDATE ACCEPT с `Update Type = combined TA/LA updating`.                                                                         | TAI:Value |
| Tx    | <a name="intraCombinedTauSuccessSmsOnly">intraCombinedTauSuccessSmsOnly</a> | Количество сообщений NAS TRACKING AREA UPDATE ACCEPT с `Update Type = combined TA/LA updating` и `Additional update result`= `SMS only`                                 | TAI:Value |
| Tx    | <a name="intraCombinedTauSuccess2">intraCombinedTauSuccess2</a>             | Количество сообщений NAS TRACKING AREA UPDATE ACCEPT с `Update Type = TA updating` и причиной "#2 IMSI unknown in HSS".                                                 | TAI:Value |
| Tx    | <a name="intraCombinedTauSuccess18">intraCombinedTauSuccess18</a>           | Количество сообщений NAS TRACKING AREA UPDATE ACCEPT с `Update Type = TA updating` и причиной "#18 CS domain not available".                                            | TAI:Value |
| Tx    | <a name="intraCombinedTauReject">intraCombinedTauReject</a>                 | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating`.                                                                         | TAI:Value |
| Tx    | <a name="intraCombinedTauReject3">intraCombinedTauReject3</a>               | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#3 Illegal UE".                                              | TAI:Value |
| Tx    | <a name="intraCombinedTauReject6">intraCombinedTauReject6</a>               | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#6 Illegal ME".                                              | TAI:Value |
| Tx    | <a name="intraCombinedTauReject7">intraCombinedTauReject7</a>               | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#7 EPS services not allowed".                                | TAI:Value |
| Tx    | <a name="intraCombinedTauReject9">intraCombinedTauReject9</a>               | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#9 UE identity cannot be derived by the network".            | TAI:Value |
| Tx    | <a name="intraCombinedTauReject10">intraCombinedTauReject10/a>              | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#10 Implicitly detached".                                    | TAI:Value |
| Tx    | <a name="intraCombinedTauReject11">intraCombinedTauReject11</a>             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#11 PLMN not allowed".                                       | TAI:Value |
| Tx    | <a name="intraCombinedTauReject12">intraCombinedTauReject12</a>             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#12 Tracking Area not allowed".                              | TAI:Value |
| Tx    | <a name="intraCombinedTauReject13">intraCombinedTauReject13</a>             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#13 Roaming not allowed in this tracking area".              | TAI:Value |
| Tx    | <a name="intraCombinedTauReject14">intraCombinedTauReject14</a>             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#14 EPS services not allowed in this PLMN".                  | TAI:Value |
| Tx    | <a name="intraCombinedTauReject15">intraCombinedTauReject15</a>             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#15 No Suitable Cells In tracking area".                     | TAI:Value |
| Tx    | <a name="intraCombinedTauReject40">intraCombinedTauReject40</a>             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#40 No EPS bearer context activated".                        | TAI:Value |
| Tx    | <a name="intraCombinedTauReject111">intraCombinedTauReject111</a>           | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#111 Protocol error, unspecified".                           | TAI:Value |
| Rx    | <a name="interCombinedTauRequest">interCombinedTauRequest</a>               | Количество сообщений inter-MME NAS TRACKING AREA UPDATE REQUEST c `Update Type = combined TA/LA updating` или `Update Type = combined TA/LA updating with IMSI attach`. | TAI:Value |
| Tx    | <a name="interCombinedTauSuccess">interCombinedTauSuccess</a>               | Количество сообщений inter-MME NAS TRACKING AREA UPDATE ACCEPT с `Update Type = combined TA/LA updating`.                                                               | TAI:Value |
| Tx    | <a name="interCombinedTauSuccessSmsOnly">interCombinedTauSuccessSmsOnly</a> | Количество сообщений inter-MME NAS TRACKING AREA UPDATE ACCEPT с `Update Type = combined TA/LA updating` и `Additional update result = SMS only`.                       | TAI:Value |
| Tx    | <a name="interCombinedTauSuccess2">interCombinedTauSuccess2</a>             | Количество сообщений NAS TRACKING AREA UPDATE ACCEPT с `Update Type = TA updating` и причиной "#2 IMSI unknown in HSS".                                                 | TAI:Value |
| Tx    | <a name="interCombinedTauSuccess18">interCombinedTauSuccess18</a>           | Количество сообщений NAS TRACKING AREA UPDATE ACCEPT с `Update Type = TA updating` и причиной "#18 CS domain not available".                                            | TAI:Value |
| Tx    | <a name="interCombinedTauReject">interCombinedTauReject</a>                 | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating`.                                                                         | TAI:Value |
| Tx    | <a name="interCombinedTauReject3">interCombinedTauReject3</a>               | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#3 Illegal UE".                                              | TAI:Value |
| Tx    | <a name="interCombinedTauReject6">interCombinedTauReject6</a>               | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#6 Illegal ME".                                              | TAI:Value |
| Tx    | <a name="interCombinedTauReject7">interCombinedTauReject7</a>               | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#7 EPS services not allowed".                                | TAI:Value |
| Tx    | <a name="interCombinedTauReject9">interCombinedTauReject9</a>               | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#9 UE identity cannot be derived by the network".            | TAI:Value |
| Tx    | <a name="interCombinedTauReject10">interCombinedTauReject10</a>             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#10 Implicitly detached".                                    | TAI:Value |
| Tx    | <a name="interCombinedTauReject11">interCombinedTauReject11</a>             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#11 PLMN not allowed".                                       | TAI:Value |
| Tx    | <a name="interCombinedTauReject12">interCombinedTauReject12</a>             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#12 Tracking Area not allowed".                              | TAI:Value |
| Tx    | <a name="interCombinedTauReject13">interCombinedTauReject13</a>             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#13 Roaming not allowed in this tracking area".              | TAI:Value |
| Tx    | <a name="interCombinedTauReject14">interCombinedTauReject14</a>             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#14 EPS services not allowed in this PLMN".                  | TAI:Value |
| Tx    | <a name="interCombinedTauReject15">interCombinedTauReject15</a>             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#15 No Suitable Cells In tracking area".                     | TAI:Value |
| Tx    | <a name="interCombinedTauReject40">interCombinedTauReject40</a>             | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#40 No EPS bearer context activated".                        | TAI:Value |
| Tx    | <a name="interCombinedTauReject111">interCombinedTauReject111</a>           | Количество сообщений NAS TRACKING AREA UPDATE REJECT с `Update Type = combined TA/LA updating` и причиной "#111 Protocol error, unspecified".                           | TAI:Value |

**Примечание.** Описание причин ошибок EMM см. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf).

### Группа ###

| Название    | Описание                                                         | Тип |
|-------------|------------------------------------------------------------------|-----|
| TAI         | Идентификатор области отслеживания. Формат:<br>`<plmn_id><tac>`. | hex |
| \<plmn_id\> | Идентификатор сети PLMN.                                         | int |
| \<tac\>     | Код области отслеживания.                                        | hex |

#### Пример ####

```
TAI:2500103E8
```

#### Пример файла ####

```csv
rx,intraTauRequest,,0
tx,intraTauSuccess,,0
tx,intraTauReject,,0
rx,periodTauRequest,,0
tx,periodTauSuccess,,0
tx,periodTauReject,,0
rx,interTauRequest,,0
tx,interTauSuccess,,0
tx,interTauReject,,0
rx,intraCombinedTauRequest,,4
rx,intraCombinedTauRequest,TAI:001010001,2
rx,intraCombinedTauRequest,TAI:001010010,2
tx,intraCombinedTauSuccess,,3
tx,intraCombinedTauSuccess,TAI:001010001,1
tx,intraCombinedTauSuccess,TAI:001010010,2
tx,intraCombinedTauReject9,,1
tx,intraCombinedTauReject9,TAI:001010001,1
rx,interCombinedTauRequest,,0
tx,interCombinedTauSuccess,,0
tx,interCombinedTauReject,,0
```