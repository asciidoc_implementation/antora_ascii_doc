---
title: "S1 Bearer Activation"
description: "Статистика процедур S1 Bearer Activation"
weight: 20
type: docs
---

Файл **<node_name>\_MME-s1BearerActivation_<datetime>_<granularity>.csv** содержит статистическую информацию по метрикам MME для процедур S1 Bearer Activation.

### Описание параметров ###

Подробную информацию см. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf).

| Tx/Rx | Метрика                                                                                         | Описание                                                                                                                             | Группа                               |
|-------|-------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------|
| Tx    | <a name="dedicatedBearerActiveRequest">dedicatedBearerActiveRequest</a>                         | Количество сообщений S1 Activate Dedicated EPS Bearer Context Request.                                                               | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="dedicatedBearerActiveSuccess">dedicatedBearerActiveSuccess</a>                         | Количество сообщений S1 Activate Dedicated EPS Bearer Context Accept.                                                                | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="dedicatedBearerActiveReject">dedicatedBearerActiveReject</a>                           | Количество сообщений S1 Activate Dedicated EPS Bearer Context Reject                                                                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="dedicatedBearerActiveReject26">dedicatedBearerActiveReject26</a>                       | Количество сообщений S1 Activate Dedicated EPS Bearer Context Reject c причиной "#26 Insufficient Resources".                        | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="dedicatedBearerActiveReject31">dedicatedBearerActiveReject31</a>                       | Количество сообщений S1 Activate Dedicated EPS Bearer Context Reject с причиной "#31 Request rejected, unspecified".                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="dedicatedBearerActiveReject43">dedicatedBearerActiveReject43</a>                       | Количество сообщений S1 Activate Dedicated EPS Bearer Context Reject с причиной "#43 Invalid EPS bearer identity".                   | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="dedicatedBearerActiveReject111">dedicatedBearerActiveReject111</a>                     | Количество сообщений S1 Activate Dedicated EPS Bearer Context Reject с причиной "#95-111 Protocol error, unspecified".               | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="dedicatedBearerActiveRequestQci1">dedicatedBearerActiveRequestQci1</a>                 | Количество сообщений S1 Activate Dedicated EPS Bearer Context Request для `QCI = 1`.                                                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="dedicatedBearerActiveRequestQci2">dedicatedBearerActiveRequestQci2</a>                 | Количество сообщений S1 Activate Dedicated EPS Bearer Context Request для `QCI = 2`.                                                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="dedicatedBearerActiveRequestQci3">dedicatedBearerActiveRequestQci3</a>                 | Количество сообщений S1 Activate Dedicated EPS Bearer Context Request для `QCI = 3`.                                                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="dedicatedBearerActiveRequestQci4">dedicatedBearerActiveRequestQci4</a>                 | Количество сообщений S1 Activate Dedicated EPS Bearer Context Request для `QCI = 4`.                                                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="dedicatedBearerActiveRequestQci65">dedicatedBearerActiveRequestQci65</a>               | Количество сообщений S1 Activate Dedicated EPS Bearer Context Request для `QCI = 65`.                                                | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="dedicatedBearerActiveRequestQci66">dedicatedBearerActiveRequestQci66</a>               | Количество сообщений S1 Activate Dedicated EPS Bearer Context Request для `QCI = 66`.                                                | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="dedicatedBearerActiveRequestQci67">dedicatedBearerActiveRequestQci67</a>               | Количество сообщений S1 Activate Dedicated EPS Bearer Context Request для `QCI = 67`.                                                | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextRequest">activateDefaultEpsBearerContextRequest</a>     | Количество сообщений S1 Activate Dedicated EPS Bearer Context Request.                                                               | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextRequest50">activateDefaultEpsBearerContextRequest50</a> | Количество сообщений S1 Activate Dedicated EPS Bearer Context Request с причиной "#50 PDN type IPv4 only allowed".                   | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextRequest51">activateDefaultEpsBearerContextRequest51</a> | Количество сообщений S1 Activate Dedicated EPS Bearer Context Request с причиной "#51 PDN type IPv6 only allowed".                   | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextRequest52">activateDefaultEpsBearerContextRequest52</a> | Количество сообщений S1 Activate Dedicated EPS Bearer Context Request с причиной "#52 Single address bearers only allowed".          | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="activateDefaultEpsBearerContextAccept">activateDefaultEpsBearerContextAccept</a>       | Количество сообщений S1 Activate Dedicated EPS Bearer Context Accept.                                                                | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextReject">activateDefaultEpsBearerContextReject</a>       | Количество сообщений S1 Activate Dedicated EPS Bearer Context Reject.                                                                | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextReject26">activateDefaultEpsBearerContextReject26</a>   | Количество сообщений S1 Activate Dedicated EPS Bearer Context Reject с причиной "#26 Insufficient resources".                        | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextReject31">activateDefaultEpsBearerContextReject31</a>   | Количество сообщений S1 Activate Dedicated EPS Bearer Context Reject с причиной "#31 Request rejected, unspecified".                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextReject47">activateDefaultEpsBearerContextReject47</a>   | Количество сообщений S1 Activate Dedicated EPS Bearer Context Reject с причиной "#47 PTI mismatch".                                  | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextReject95">activateDefaultEpsBearerContextReject95</a>   | Количество сообщений S1 Activate Dedicated EPS Bearer Context Reject с причиной "#95 Semantically incorrect message".                | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextReject96">activateDefaultEpsBearerContextReject96</a>   | Количество сообщений S1 Activate Dedicated EPS Bearer Context Reject с причиной "#96 Invalid mandatory information".                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextReject111">activateDefaultEpsBearerContextReject111</a> | Количество сообщений S1 Activate Dedicated EPS Bearer Context Reject с причиной "#95-111 Protocol error, unspecified".               | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="pdnConnectivityRequest">pdnConnectivityRequest</a>                                     | Количество сообщений S1 PDN Connectivity Request.                                                                                    | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject">pdnConnectivityReject</a>                                       | Количество сообщений S1 PDN Connectivity Reject.                                                                                     | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject8">pdnConnectivityReject8</a>                                     | Количество сообщений S1 PDN Connectivity Reject с причиной "#8 Operator determined barring".                                         | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject26">pdnConnectivityReject26</a>                                   | Количество сообщений S1 PDN Connectivity Reject с причиной "#26 Insufficient resources".                                             | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject27">pdnConnectivityReject27</a>                                   | Количество сообщений S1 PDN Connectivity Reject с причиной "#27 Missing or unknown APN".                                             | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject28">pdnConnectivityReject28</a>                                   | Количество сообщений S1 PDN Connectivity Reject с причиной "#28 Unknown PDN type".                                                   | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject29">pdnConnectivityReject29</a>                                   | Количество сообщений S1 PDN Connectivity Reject с причиной "#29 User authentication failed".                                         | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject30">pdnConnectivityReject30</a>                                   | Количество сообщений S1 PDN Connectivity Reject с причиной "#30 Request rejected by Serving GW or PDN GW".                           | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject31">pdnConnectivityReject31</a>                                   | Количество сообщений S1 PDN Connectivity Reject с причиной "#31 Request rejected, unspecified".                                      | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject32">pdnConnectivityReject32</a>                                   | Количество сообщений S1 PDN Connectivity Reject с причиной "#32 Service option not supported".                                       | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject33">pdnConnectivityReject33</a>                                   | Количество сообщений S1 PDN Connectivity Reject с причиной "#33 Requested service option not subscribed".                            | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject34">pdnConnectivityReject34</a>                                   | Количество сообщений S1 PDN Connectivity Reject с причиной "#34 Service option temporarily out of order".                            | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject35">pdnConnectivityReject35</a>                                   | Количество сообщений S1 PDN Connectivity Reject с причиной "#35 PTI already in use".                                                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject38">pdnConnectivityReject38</a>                                   | Количество сообщений S1 PDN Connectivity Reject с причиной "#38 Network failure".                                                    | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject41">pdnConnectivityReject41</a>                                   | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#41 Semantic error in the TFT operation".                                | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject42">pdnConnectivityReject42</a>                                   | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#42 Syntactical error in the TFT operation".                             | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject44">pdnConnectivityReject44</a>                                   | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#44 Semantic errors in packet filter(s)".                                | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject45">pdnConnectivityReject45</a>                                   | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#45 Syntactical error in packet filter(s)".                              | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject50">pdnConnectivityReject50</a>                                   | Количество сообщений S1 PDN Connectivity Reject с причиной "#50 PDN type IPv4 only allowed".                                         | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject51">pdnConnectivityReject51</a>                                   | Количество сообщений S1 PDN Connectivity Reject с причиной "#51 PDN type IPv6 only allowed".                                         | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject54">pdnConnectivityReject54</a>                                   | Количество сообщений S1 PDN Connectivity Reject с причиной "#54 PDN connection does not exist".                                      | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject55">pdnConnectivityReject55</a>                                   | Количество сообщений S1 PDN Connectivity Reject с причиной "#55 Multiple PDN connections for a given APN not allowed".               | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject56">pdnConnectivityReject56</a>                                   | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#56 Collision with network initiated request".                           | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject60">pdnConnectivityReject60</a>                                   | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#60 Bearer handling not supported".                                      | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject65">pdnConnectivityReject65</a>                                   | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#65 Maximum number of EPS bearers reached".                              | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject96">pdnConnectivityReject96</a>                                   | Количество сообщений S1 PDN Connectivity Reject с причиной "#96 Invalid mandatory information".                                      | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject111">pdnConnectivityReject111</a>                                 | Количество сообщений S1 PDN Connectivity Reject с причиной "#95-111 Protocol error, unspecified".                                    | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject112">pdnConnectivityReject112</a>                                 | Количество сообщений S1 PDN Connectivity Reject с причиной "#112 APN restriction value incompatible with active EPS bearer context". | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject113">pdnConnectivityReject113</a>                                 | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#113 Multiple accesses to a PDN connection not allowed".                 | TAI:Value, IMSIPLMN:Value, APN:Value |

### Группа ###

| Название    | Описание                                                                             | Тип    |
|-------------|--------------------------------------------------------------------------------------|--------|
| TAI         | Идентификатор области отслеживания. Формат:<br>`<plmn_id><tac>`.                     | hex    |
| \<plmn_id\> | Идентификатор сети PLMN.                                                             | int    |
| \<tac\>     | Код области отслеживания.                                                            | hex    |
| IMSIPLMN    | Идентификатор сети PLMN на основе первых 5 цифр из номера IMSI.                      | int    |
| APN         | Идентификатор области отслеживания. Формат:<br>`<name>.mnc<mnc>.mcc<mcc>.<network>`. | string |
| \<name\>    | Имя точки доступа.                                                                   | string |
| \<mnc\>     | Код мобильной сети, <abbr title="Mobile Network Code">MNC</abbr>.                    | int    |
| \<mcc\>     | Мобильный код страны, <abbr title="Mobile Country Code">MCC</abbr>.                  | int    |
| \<network\> | Тип сети.                                                                            | string |

**Примечание.** Описание причин ошибок ESM см. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf).

#### Пример ####

```
TAI,2500103E8
IMSIPLMN,25099
APN:internet.mnc01.mcc250.gprs
```

#### Пример файла ####

```csv
tx,dedicatedBearerActiveRequest,,5
tx,dedicatedBearerActiveRequest,APN:ims.mnc001.mcc001.gprs,5
tx,dedicatedBearerActiveRequest,IMSIPLMN:00101,5
tx,dedicatedBearerActiveRequest,TAI:001010001,5
tx,dedicatedBearerActiveRequestQci1,,5
tx,dedicatedBearerActiveRequestQci1,APN:ims.mnc001.mcc001.gprs,5
tx,dedicatedBearerActiveRequestQci1,IMSIPLMN:00101,5
tx,dedicatedBearerActiveRequestQci1,TAI:001010001,5
rx,dedicatedBearerActiveSuccess,,2
rx,dedicatedBearerActiveSuccess,APN:ims.mnc001.mcc001.gprs,2
rx,dedicatedBearerActiveSuccess,IMSIPLMN:00101,2
rx,dedicatedBearerActiveSuccess,TAI:001010001,2
rx,dedicatedBearerActiveReject,,0
tx,activateDefaultEpsBearerContextRequest,,12
tx,activateDefaultEpsBearerContextRequest,APN:a.mnc093.mcc208.gprs,4
tx,activateDefaultEpsBearerContextRequest,APN:ims.mnc001.mcc001.gprs,4
tx,activateDefaultEpsBearerContextRequest,APN:internet.mnc001.mcc001.gprs,4
tx,activateDefaultEpsBearerContextRequest,IMSIPLMN:00101,8
tx,activateDefaultEpsBearerContextRequest,IMSIPLMN:20893,4
tx,activateDefaultEpsBearerContextRequest,TAI:001010001,8
tx,activateDefaultEpsBearerContextRequest,TAI:208930001,4
rx,activateDefaultEpsBearerContextAccept,,12
rx,activateDefaultEpsBearerContextAccept,APN:a.mnc093.mcc208.gprs,4
rx,activateDefaultEpsBearerContextAccept,APN:ims.mnc001.mcc001.gprs,4
rx,activateDefaultEpsBearerContextAccept,APN:internet.mnc001.mcc001.gprs,4
rx,activateDefaultEpsBearerContextAccept,IMSIPLMN:00101,8
rx,activateDefaultEpsBearerContextAccept,IMSIPLMN:20893,4
rx,activateDefaultEpsBearerContextAccept,TAI:001010001,8
rx,activateDefaultEpsBearerContextAccept,TAI:208930001,4
rx,pdnConnectivityRequest,,13
rx,pdnConnectivityRequest,APN:a.mnc093.mcc208.gprs,4
rx,pdnConnectivityRequest,APN:ims.mnc001.mcc001.gprs,4
rx,pdnConnectivityRequest,APN:internet.mnc001.mcc001.gprs,4
rx,pdnConnectivityRequest,IMSIPLMN:00101,8
rx,pdnConnectivityRequest,IMSIPLMN:20893,4
rx,pdnConnectivityRequest,IMSIPLMN:25002,1
rx,pdnConnectivityRequest,TAI:001010001,8
rx,pdnConnectivityRequest,TAI:001010051,1
rx,pdnConnectivityRequest,TAI:208930001,4
tx,pdnConnectivityReject,,0
```
